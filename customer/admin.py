from django.contrib import admin
from django.contrib.admin import AdminSite
from django.utils.html import format_html
from tbot.admin_bot import AdminBot

from .models import *

adminBot = AdminBot()

class Request(admin.StackedInline):
    model = Req

def block(modeladmin, request, queryset):
    queryset.update(active=False)

block.short_description = 'Заблокировать'

@admin.register(Telegram_user)
class UserAdmin(admin.ModelAdmin):
    list_display = ("telegram_id", "username", 'show_firm_url',"created_at")
    def show_firm_url(self, obj):
        return format_html("<a href='{url}'>{count}</a>", url=obj.url(), count=obj.count())

    show_firm_url.short_description = 'Заявки'

    show_firm_url.allow_tags = True
    inlines = [Request]
    actions = [block]


@admin.register(Product_cart)
class UserAdmin(admin.ModelAdmin):
    pass

@admin.register(Greetings)
class UserAdmin(admin.ModelAdmin):
    pass

@admin.register(Req)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id','tg_id','name', "type", "status", "state", 'created_at')
    def tg_id(self, obj):
        if obj.user:
            return obj.user.telegram_id
        else:
            return '-'

    tg_id.short_description = 'Chat ID'

    def name(self, obj):
        if obj.user:
            return obj.user.username
        else:
            return '-'

    name.short_description = 'Имя пользователя'





class HotelAdminSite(AdminSite):
    site_header = "Admin"
    site_title = "Виписки з реєстрів"
    index_title = 'Виписки з реєстрів'

event_admin_site = HotelAdminSite(name='admin')






admin.site.site_title = 'Виписки з реєстрів'
admin.site.site_header = 'Виписки з реєстрів'
