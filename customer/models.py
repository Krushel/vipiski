from django.db import models
from tbot.models import BotConfig
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save,pre_delete,post_init
from django.db.models import ProtectedError
from django.core.exceptions import ValidationError, SuspiciousOperation, ObjectDoesNotExist, PermissionDenied, MultipleObjectsReturned
from django import forms
from django.contrib.auth.models import User
import os

ROLES = [
    ("Менеджер", "Менеджер"),
    ("Пользователь", "Пользователь")
]

class Telegram_user(models.Model):
    """Класс описує модель користувача і співробітника"""
    telegram_id = models.CharField('Chat ID',max_length=64, blank=True, null=True, default="")
    username = models.CharField("Имя пользователя",max_length=64, blank=True, null=True, default="")
    full_name = models.CharField("Имя в телеграм", max_length=64, blank=True, null=True, default="")
    name = models.CharField("Имя", max_length=64, blank=True, null=True, default="")
    phone_number = models.CharField("Номер телефона", max_length=64, blank=True, null=True, default="")
    state = models.CharField("Статус", max_length=64, blank=True, null=True, default="")
    role = models.CharField("Роль пользователя", max_length=64, blank=True, null=True, default="Пользователь", choices=ROLES)
    text = models.CharField("Текст заявки", max_length=64, blank=True, null=True, default="")
    type = models.CharField("Тип заявки", max_length=64, blank=True, null=True, default="")
    created_at = models.DateTimeField('Дата регистрации',auto_now_add=True)
    active = models.BooleanField("Активен", default=True)

    def count(self):
        return Req.objects.filter(user=self).count()

    def url(self):
        return f"http://127.0.0.1:8000/admin/customer/req/?user__id__exact={self.pk}"

    def __str__(self):
        return f"{self.username}"

    class Meta:
        ordering = ["pk"]
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

class Product_cart(models.Model):
    """Класс описує модель квартири"""
    name = models.CharField("Название", max_length=300, blank=True, null=True, default="")
    description = models.TextField("Краткое описание", blank=True, null=True, default="")
    price = models.DecimalField("Цена", blank=True, null=True, default=0, decimal_places=2, max_digits=8)
    quantity = models.IntegerField("Количество", blank=True, null=True, default=0)
    currency = models.CharField("Валюта", max_length=300, blank=True, null=True, default="UAH")

    def __str__(self):
        return f"{self.name}"

    class Meta:
        ordering = ["pk"]
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"

class Req(models.Model):
    """Класс описує модель квартири"""
    type = models.CharField("Параметр поиска", max_length=300, blank=True, null=True, default="")
    text = models.TextField("Текст", blank=True, null=True, default="")
    user = models.ForeignKey(Telegram_user, blank=True, on_delete=models.SET_NULL, null=True)
    file = models.FileField('Файл PDF', blank=True, help_text='Загрузите результат анализа, до 5 mb')
    state = models.CharField("Статус заяки", max_length=64, blank=True, null=True, default="Новая")
    status = models.CharField("Статус оплаты", max_length=64, blank=True, null=True, default="Оплачена")
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)

    def __str__(self):
        return f"{self.text}"

    class Meta:
        ordering = ["pk"]
        verbose_name = "Заявка"
        verbose_name_plural = "Заявки"


class Greetings(models.Model):
    """Класс описує модель квартири"""
    text = models.TextField("Текст", blank=True, null=True, default="")

    def __str__(self):
        return f"{self.text}"

    class Meta:
        ordering = ["pk"]
        verbose_name = "Приветствие"
        verbose_name_plural = "Приветствия"

from tbot.admin_bot import AdminBot
adminBot = AdminBot()

@receiver(pre_save)
def Request_save(sender, instance, **kwargs):
    if sender == Req:
        if instance.file:
            instance.state = "Выполнена"
            adminBot.send_file(instance.user.telegram_id, f'Анализ согласно заявки №{instance.id} успешно выполнен.',
                               instance.file, 'Главное меню', 'back_to_menu')