# Create your tasks here
from telebot.types import *
from loguru import logger
from celery import shared_task
from .models import *
from datetime import datetime, timedelta, time, date
from tbot.admin_bot import AdminBot
from django.core.files import File
from django.db.models import Sum

adminBot = AdminBot()

@shared_task
def report_about_subscription():
    abonements = Abonement.objects.all()
    logger.info(abonements)
    for abonement in abonements:
        if (date.today() - abonement.date)<timedelta(days=30):
            adminBot.send_text(abonement.user, 'До конца срока действия вашего абонемента осталось 30 дней!',
                               'Главное меню', 'back_to_menu')
            abonement.delete()
    logger.info("send notify")
    return "send notify"

@shared_task
def report_about_certificate():
    cert = Certif.objects.all()
    logger.info(cert)
    for c in cert:
        if (date.today() - c.date)<timedelta(days=69):
            adminBot.send_text(c.user, 'До конца срока действия вашего сертификата остался 21 день!',
                               'Главное меню', 'back_to_menu')
            c.delete()
    logger.info("send notify")
    return "send notify"

@shared_task
def reload_prices():
    import datetime

    import requests, json

    url = "https://api.yclients.com/api/v1/book_services/198566"

    header = {'Accept': 'application/vnd.yclients.v2+json', 'Authorization': 'h4h7ka8f7azpksmt8wre',
              "Content-type": "application/json"}

    response_decoded_json = requests.get(url, headers=header)
    response_json = response_decoded_json.json()
    print(response_json)
    price = Price.objects.all().first()
    float = Floating.objects.get(name__icontains='Флоатинг Стандарт')
    floatd = Floating.objects.get(name__icontains='Флоатинг для двоих')
    for service in response_json['data']['services']:
        if service['id'] == 2972568:
            price.priceStandart = service['price_max']
            float.price = service['price_max']
        if service['id'] == 2972569:
            price.priceDouble = service['price_max']
            floatd.price = service['price_max']
    price.save()
    float.save()
    floatd.save()
