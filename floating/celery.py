import os
from celery.schedules import crontab
from celery import Celery
from .settings import DEV_MODE

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')
CELERY_BROKER_URL = 'amqp://guest@localhost//' if DEV_MODE else "redis://default:6379/0"
app = Celery('app', broker=CELERY_BROKER_URL)
print(1)

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = {
    'report_about_subscription': {
        'task': 'customer.tasks.report_about_subscription',
        'schedule': crontab(hour=10, minute=0),
    },'report_about_certificate': {
        'task': 'customer.tasks.report_about_certificate',
        'schedule': crontab(hour=10, minute=15),
    },'reload_prices': {
        'task': 'customer.tasks.reload_prices',
        'schedule': crontab(hour=10, minute=50),
    }}

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()