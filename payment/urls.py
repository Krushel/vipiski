from django.urls import path
from . import payment

urlpatterns = [
    path('', payment.pay_callback, name="pay_callback"),
]
