import os
import datetime
from enum import Enum
import time
from telebot import types
from django.utils import timezone
from django.conf import settings
from django.core.files.base import ContentFile
import requests
import json

from .tbot import TBot
from .keyboards import *
from .customer_bot import *
from customer.models import *


customerBot = CustomerBot()


class AdminBot(TBot):

    def __init__(self):
        super().__init__()

    def get_file(self, message):
        name = message.document.file_name
        info = self.bot.get_file(str(message.document.file_id))
        file = self.bot.download_file(info.file_path)
        return [name, file]

    def get_photo(self, message):
        name = str(message.photo[-1].file_id)+".jpg"
        info = self.bot.get_file(str(message.photo[-1].file_id))
        file = self.bot.download_file(info.file_path)
        return [name, file]

    def send_text(self, chat_id, text, btn=None, call=None, resize=True, contact=False, one_time=False):
        if call and btn:
            if type(btn) == str:
                keyboard = types.InlineKeyboardMarkup()
                Button = types.InlineKeyboardButton(text=btn, callback_data=call)
                keyboard.add(Button)
            elif call == 'inline':
                keyboard = types.InlineKeyboardMarkup()
                Button = types.InlineKeyboardButton(text=btn, switch_inline_query_current_chat='')
                keyboard.add(Button)
            else:
                keyboard = types.InlineKeyboardMarkup()
                for i in range(len(btn)):
                    if call[i] == 'inline':
                        Button = types.InlineKeyboardButton(text=btn[i], switch_inline_query_current_chat='')
                    else:
                        Button = types.InlineKeyboardButton(text=btn[i], callback_data=call[i])
                    keyboard.add(Button)
        elif not call and btn:
            if type(btn) == str:
                keyboard = types.ReplyKeyboardMarkup(resize_keyboard=resize, one_time_keyboard=one_time)
                buttonDogovor = types.KeyboardButton(text=btn, request_contact=contact)
                keyboard.add(buttonDogovor)
            else:
                keyboard = types.ReplyKeyboardMarkup(resize_keyboard=resize, one_time_keyboard=one_time)
                for i in range(len(btn)):
                    Button = types.KeyboardButton(text=btn[i])
                    keyboard.add(Button)
        else:
            keyboard=None

        self.bot.send_message(
            chat_id=chat_id,
            text=text,
            reply_markup=keyboard,
            parse_mode="HTML")


    def send_menu(self, chat_id, no_state=False):

        if not no_state:
            user = customerBot.get_user(chat_id)
            user.state = 'menu'
            user.save()

        text = 'Вибери параметри, за якими хочете здійснити пошук:\n<b>За адресою</b> - пошук за адресою\n' \
               '<b>За реєстр. номером</b>  - пошук за реєстраційним номером об`єкта нерухомості.\n<b>За кадастр. номером</b> - пошук за кадастровим номером земельної дилянка\n<b>Фіз./Юр. особа</b> - пошук за даними фіз. особи або юр. особи.'
        keyboard = types.InlineKeyboardMarkup()
        buttonTaxCredit = types.InlineKeyboardButton(text='За адресою', callback_data='address')
        buttonCard = types.InlineKeyboardButton(text='За реєстр. номером', callback_data='r_number')
        buttonCash = types.InlineKeyboardButton(text='За кадастр. номером', callback_data='k_number')
        buttonReks = types.InlineKeyboardButton(text='Фіз./Юр. особа', callback_data='osoba')
        keyboard.row(buttonTaxCredit)
        keyboard.row(buttonCard)
        keyboard.row(buttonCash)
        keyboard.row(buttonReks)

        self.bot.send_message(
            chat_id=chat_id,
            text=text,
            reply_markup=keyboard,
            parse_mode="HTML"
        )

    def send_file(self, chat_id, text, file, btn=None, call=None, resize=True, contact=False, one_time=False):

        if call and btn:
            if type(btn) == str:
                keyboard = types.InlineKeyboardMarkup()
                Button = types.InlineKeyboardButton(text=btn, callback_data=call)
                keyboard.add(Button)
            else:
                keyboard = types.InlineKeyboardMarkup()
                for i in range(len(btn)):
                    Button = types.InlineKeyboardButton(text=btn[i], callback_data=call[i])
                    keyboard.add(Button)
        elif not call and btn:
            if type(btn) == str:
                keyboard = types.ReplyKeyboardMarkup(resize_keyboard=resize, one_time_keyboard=one_time)
                buttonDogovor = types.KeyboardButton(text=btn, request_contact=contact)
                keyboard.add(buttonDogovor)
            else:
                keyboard = types.ReplyKeyboardMarkup(resize_keyboard=resize, one_time_keyboard=one_time)
                for i in range(len(btn)):
                    Button = types.KeyboardButton(text=btn[i])
                    keyboard.add(Button)
        else:
            keyboard = None

        self.bot.send_document(
            chat_id=chat_id,
            caption=text,
            data=file,
        reply_markup=keyboard,
            parse_mode="HTML", )


    def send_req(self, id):
        req = Req.objects.get(pk=id)
        manager = Telegram_user.objects.filter(role = 'Менеджер')[0]
        keyboard = types.InlineKeyboardMarkup()
        # Button = types.InlineKeyboardButton(text='Перейти', url='http://' + os.getenv('DOMAIN') + f'/admin/customer/req/{id}/change/')
        Button1 = types.InlineKeyboardButton(text='Файл', callback_data=f'file|{id}')
        # keyboard.add(Button)
        keyboard.add(Button1)
        self.bot.send_message(manager.telegram_id, f'Получена новая заявка(#{id})!\nДетали можете просмотреть, '
                                            f'нажав на кнопку "Перейти"', reply_markup=keyboard)