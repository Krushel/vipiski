from django.apps import AppConfig


class TbotConfig(AppConfig):
    name = 'tbot'
    verbose_name = "Настройки бота"
