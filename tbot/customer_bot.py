import os
import random
import re
import string
import time
from enum import Enum

from telebot import types
from django.utils import timezone
import io
from django.core.files.images import ImageFile
from django.conf import settings
import requests
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.core.mail import send_mail
from django.conf import settings
from customer.models import *
from .tbot import TBot


class CustomerBot(TBot):

    def __init__(self):
        super().__init__()

    # def get_employees(self):
    #     """Перевіряємо чи існує юзер в базі."""
    #     users = Telegram_user.objects.filter(is_employee=True)
    #     return users
    #
    # def get_hotels(self, query):
    #     """Перевіряємо чи існує юзер в базі."""
    #     hotels = Hotel.objects.filter(yur_name__icontains = query)
    #     return hotels[0:50]


    def add_new_user(self, message):
        """Додаємо нового користувача."""
        telegram_id = message.from_user.id
        username = f"@{message.from_user.username}"
        full_name = f"{message.from_user.first_name} {message.from_user.last_name}" if message.from_user.last_name else f"{message.from_user.first_name}"
        return Telegram_user.objects.create(telegram_id=telegram_id, username=username, full_name=full_name, role="Пользователь")

    def add_cart_product(self, user, product, type=False):
        """Додаємо нового користувача."""
        cart = Cart.objects.get_or_create(user=user)[0]
        if type in ('dok', 'banka'):
            image = product.image
        description = product.description
        name = product.name
        price = product.price
        dok = False
        banka = False
        plastic = False
        digital = False
        subscription = False
        if type == 'dok':
            dok = True
        elif type == 'banka':
            banka = True
        elif type == 'digital':
            digital = True
        elif type == 'plastic':
            plastic = True
        elif type == 'subscription':
            subscription = True
        quantity = 1
        cart_product = cart.product.filter(name=name, price=price, dok=dok, banka=banka)
        if cart_product:
            cart_product[0].quantity+=1
            cart_product[0].save()
        else:
            if type in ('dok', 'banka'):
                product = Product_cart.objects.create(image=image, name=name, description=description, price=price,
                                              dok=dok, banka=banka, quantity=quantity)
            else:
                product = Product_cart.objects.create(name=name, price=price, quantity=quantity, description=description,
                                                      digital=digital, plastic=plastic, subscription=subscription)
            cart.product.add(product)

    # def add_new_hotel_req(self, user, img):
    #     """Додаємо нового користувача."""
    #     userb = user.telegram_id
    #     yur_name = user.yur_name
    #     name = user.name
    #     phone = user.phone_number
    #     dolzhnost = user.dolzhnost
    #     email = user.email
    #     site = user.site
    #     image = ImageFile(io.BytesIO(img[1]), name=img[0])
    #     city = user.city
    #     req = Hotel_request.objects.create(user_id=userb, yur_name=yur_name, phone=phone, dolzhnost=dolzhnost, email=email,
    #                                     site=site, name=name, city=city)
    #     photo = HotelRequestImage.objects.create(post=req, images=image)
    #     req.state = 'Получена'
    #     return req.save()
    #
    # def add_new_hotel_cv(self, img):
    #     """Додаємо нового користувача."""
    #     image = ImageFile(io.BytesIO(img[1]), name=img[0])
    #     req = Hotel_request.objects.create()
    #     photo = HotelRequestImage.objects.create(post=req, images=image)
    #     req.state = 'На модерации'
    #     return req.save()

    # def add_new_hotel(self, request):
    #     """Додаємо нового користувача."""
    #     userb = request.user_id
    #     yur_name = request.yur_name
    #     name = request.name
    #     phone = request.phone
    #     dolzhnost = request.dolzhnost
    #     email = request.email
    #     site = request.site
    #     image = HotelRequestImage.objects.get(post=request)
    #     image = image.images
    #     city = request.city
    #     moder = request.moder
    #     hotel_address = request.hotel_address
    #     hotel_family = request.hotel_family
    #     password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
    #     user = User.objects.create_user(username=userb,
    #                                     email=email,
    #                                     password=password, is_staff=True, last_name=password)
    #     new_group = Group.objects.get_or_create(name='Hotels')
    #     group = Group.objects.get(name='Hotels')
    #     user.groups.add(group)
    #     subject = f'Thank you for registering to our site'
    #     message=f'Hey!\nI`m the telegram bot Floppy, the first hotel search bot in the world!\nI unite partners and tourists all over the world that` why I invite your company to join us!\nYou already have an account on our service\n• https://floppy-travel.com\n• {user.username}\n• {password}\nIn your account,you can enter the following information:\n- hotel information\n- full and short description of the hotel\n- hotel contacts\n- contacts of authorized employees for communication with travel agents or tourists\n\nI`ll be happy to help and answer any questions @floppy_travel_bot'
    #     email_from = settings.EMAIL_HOST_USER
    #     recipient_list = [email,]
    #     send_mail(subject, message, email_from, recipient_list)
    #     req = Hotel.objects.create(user_id=userb, yur_name=yur_name, phone=phone, dolzhnost=dolzhnost, email=email,
    #                                     site=site, name=name, city=city, owner=user, moder=moder, hotel_address=hotel_address, hotel_family=hotel_family)
    #     photo = HotelImage.objects.create(post=req, images=image)
    #     req.state = 'Получена'
    #     return req.save()

    def check_user_dont_exists(self, message):
        """Перевіряємо чи існує юзер в базі."""
        telegram_id = message.from_user.id
        user = Telegram_user.objects.filter(telegram_id=telegram_id).count()
        if user == 0:
            return True
        else:
            return False

    def get_user(self, telegram_id):
        """Перевіряємо роль користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        return user

    # def get_hotel(self, yur_name):
    #     """Перевіряємо роль користувача."""
    #     hotel = Hotel.objects.get(yur_name=yur_name)
    #     return hotel
    #
    # def get_hotel_by_pk(self,pk):
    #     hotel = Hotel.objects.get(pk=pk)
    #     return hotel
    #
    # def get_hotel_employees(self, yur_name):
    #     """Перевіряємо роль користувача."""
    #     employees = Employee.objects.filter(hotel__yur_name=yur_name)
    #     return employees
    #
    # def get_hotel_image(self, hotel):
    #     """Перевіряємо роль користувача."""
    #     image = HotelImage.objects.filter(post=hotel)
    #     image = image[0].images
    #     return image
    #
    # def get_operator_reques(self, telegram_id):
    #     """Перевіряємо роль користувача."""
    #     user = Operator_request.objects.get(id=telegram_id)
    #     return user
    #
    # def get_hotel_reques(self, telegram_id):
    #     """Перевіряємо роль користувача."""
    #     user = Hotel_request.objects.get(id=telegram_id)
    #     return user
    #
    # def get_hotel_request_image(self, req):
    #     """Перевіряємо роль користувача."""
    #     image = HotelRequestImage.objects.get(post=req)
    #     image = image.images
    #     return image
    #
    # def get_oper_request_image(self, req):
    #     """Перевіряємо роль користувача."""
    #     image = OperImage.objects.get(post=req)
    #     image = image.images
    #     return image
    #
    # def check_user_lang(self, telegram_id):
    #     """Перевіряємо роль користувача."""
    #     user = Telegram_user.objects.get(telegram_id=telegram_id)
    #     return user.language
    #
    def check_user_role(self, telegram_id):
        """Перевіряємо роль користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        return user.role

    def check_user_state(self, telegram_id):
        """Перевіряємо стан користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        return user.state
    #
    # def check_user_phone(self, telegram_id):
    #     """Перевіряємо роль користувача."""
    #     user = Telegram_user.objects.get(telegram_id=telegram_id)
    #     return user.phone_number
    #
    # def check_user_city(self, telegram_id):
    #     """Перевіряємо роль користувача."""
    #     user = Telegram_user.objects.get(telegram_id=telegram_id)
    #     return user.city
    #
    # def check_user_yur(self, telegram_id):
    #     """Перевіряємо роль користувача."""
    #     user = Telegram_user.objects.get(telegram_id=telegram_id)
    #     return user.yur_name
    #
    # def check_user_email(self, telegram_id):
    #     """Перевіряємо роль користувача."""
    #     user = Telegram_user.objects.get(telegram_id=telegram_id)
    #     return user.email
    #
    # def check_user_name(self, telegram_id):
    #     """Перевіряємо роль користувача."""
    #     user = Telegram_user.objects.get(telegram_id=telegram_id)
    #     return user.name

    def check_user_dolzhnost(self, telegram_id):
        """Перевіряємо роль користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        return user.dolzhnost

    def check_user_dolzhnost(self, telegram_id):
        """Перевіряємо роль користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        return user.dolzhnost

    def check_user_site(self, telegram_id):
        """Перевіряємо роль користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        return user.site






    def set_user_state(self, telegram_id, state):
        """"Встановлюємо стейт користувачу"""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.state = state
        return user.save()

    def set_user_role(self, telegram_id, role):
        """"Встановлюємо стейт користувачу"""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.role = role
        return user.save()


    def set_user_lang(self, telegram_id, lang):
        """"Встановлюємо стейт користувачу"""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.language = lang
        return user.save()

    def set_user_phone(self, telegram_id, phone):
        """
        Записуємо номер телефону користувача,
        закінчуємо реєстрацію.
        """
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.phone_number = phone
        return user.save()

    def set_user_city(self, telegram_id, city):
        """"Встановлюємо стейт користувачу"""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.city = city
        return user.save()

    def set_user_yur(self, telegram_id, yur):
        """"Встановлюємо стейт користувачу"""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.yur_name = yur
        return user.save()

    def set_user_email(self, telegram_id, email):
        """"Встановлюємо стейт користувачу"""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.email = email
        return user.save()

    def set_user_name(self, telegram_id, name):
        """Записуємо імя користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.name = name
        return user.save()

    def set_user_dolzhnost(self, telegram_id, dolzhnost):
        """Записуємо імя користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.dolzhnost = dolzhnost
        return user.save()

    def set_user_site(self, telegram_id, site):
        """Записуємо імя користувача."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.site = site
        return user.save()

    def set_operator_request_state(self, id, state):
        """Записуємо імя користувача."""
        req = Operator_request.objects.get(id=id)
        req.state = state
        return req.save()

    def set_hotel_request_state(self, id, state):
        """Записуємо імя користувача."""
        req = Hotel_request.objects.get(id=id)
        req.state = state
        return req.save()


    def set_operator_request_moder(self, id, moder):
        """Записуємо імя користувача."""
        req = Operator_request.objects.get(id=id)
        req.moder = moder
        return req.save()

    def set_hotel_request_moder(self, id, moder):
        """Записуємо імя користувача."""
        req = Hotel_request.objects.get(id=id)
        req.moder = moder
        return req.save()

    def set_operator_request_messages_to_del(self, id, msgs):
        """Записуємо імя користувача."""
        req = Operator_request.objects.get(id=id)
        req.messages_to_del = msgs
        req.messages_to_del = msgs
        return req.save()

    def set_hotel_request_messages_to_del(self, id, msgs):
        """Записуємо імя користувача."""
        req = Hotel_request.objects.get(id=id)
        req.messages_to_del = msgs
        req.messages_to_del = msgs
        return req.save()

    def add_oper_request_message(self, id, message):
        """Записуємо імя користувача."""
        req = Operator_request.objects.get(id=id)
        req.messages_to_del += message
        return req.save()

    def add_hotel_request_message(self, id, message):
        """Записуємо імя користувача."""
        req = Hotel_request.objects.get(id=id)
        req.messages_to_del += message
        return req.save()

    def set_hotel_request_reason(self, id, reason):
        """Записуємо імя користувача."""
        req = Hotel_request.objects.get(id=id)
        req.reason = reason
        req.reason = reason
        return req.save()

    def set_operator_request_reason(self, id, reason):
        """Записуємо імя користувача."""
        req = Operator_request.objects.get(id=id)
        req.reason = reason
        req.reason = reason
        return req.save()






    def set_interested_district(self, telegram_id, dist):
        """Встановлюємо чим цікавиться юзер."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.interested_dist = dist
        return user.save()


    def check_phone_number(self, message):
        """Перевірка номер телефону"""
        string = message.text
        match1 = re.fullmatch(r'\+\d{12}', string)
        match2 = re.fullmatch(r'\+\d{3}\s\d{2}\s\d{3}\s\d{2}\s\d{2}', string)
        match3 = re.fullmatch(r'\d\s\d{2}\s\d{3}\s\d{2}\s\d{2}', string)
        match4 = re.fullmatch(r'\d{10}', string)
        match5 = re.fullmatch(r'\d{3}\s\d{2}\s\d{3}\s\d{2}\s\d{2}', string)
        match6 = re.fullmatch(r'\d{12}', string)
        match7 = re.fullmatch(r'\d{11}', string)
        return [match1, match2, match3, match4, match5, match6, match7]

    def set_cost_range(self, telegram_id, cost):
        """Встановлюємо діапазон цін."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.cost_range = cost
        return user.save()

    def create_new_review_request(self, call):
        """Створюємо нову заявку на перегляд квартири користувачем."""
        user = Telegram_user.objects.get(telegram_id=call.from_user.id)
        apartment_id = int(call.data.split()[-1])
        apartment = Apartment.objects.get(id=apartment_id)
        if user.refferal:
            raw_date = timezone.localtime(timezone.now())
            today_date = timezone.make_naive(raw_date)
            today_date = today_date.date()
            review = ReviewRequest(customer=user, apartment=apartment, employee=user.refferal, created=today_date)
            review.save()
            self.send_employees_review_requests(call, employee_chat=user.refferal.telegram_id, review=review)
        else:
            raw_date = timezone.localtime(timezone.now())
            today_date = timezone.make_naive(raw_date)
            today_date = today_date.date()
            review = ReviewRequest(customer=user, apartment=apartment, created=today_date)
            review.save()
            admins = Telegram_user.objects.filter(role__in=["СуперАдмин", "Администратор"])
            for admin in admins:
                self.send_employees_review_requests(call, employee_chat=admin.telegram_id, review=review)

    def change_price_to_all_apartments(self, user_liked):
        """Перераховуємо ціни квартир згідно курсу."""
        usd = self.get_exchange_rate()
        apartments = Apartment.objects.filter(district=user_liked, is_active=True)
        for apartment in apartments:
            apartment.price_usd = int(int(apartment.price_uah) / usd)
            apartment.save()

    def get_exchange_rate(self):
        """Робимо запит на мінфін і дістаємо курс долара."""
        r = requests.get(url=self.URL, headers=self.HEADERS)
        soup = BeautifulSoup(r.text, 'html.parser')
        currency = soup.find("td", class_="responsive-hide td-collapsed mfm-text-nowrap mfm-text-right").get_text(strip=True)
        return float(currency[:7])

    def set_work_field_2(self, telegram_id, text):
        """Записує id повідомлень для подальшого видалення."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user.messages_to_del = text
        return user.save()

    def delete_apartment_messages_back_button_customer(self, call):
        """Видаляємо усі повідомлення з квартирами при Назад."""
        user = Telegram_user.objects.get(telegram_id=call.from_user.id)
        messages = user.messages_to_del.split("|")
        messages = messages[:len(messages)-1]
        for message in messages:
            try:
                self.bot.delete_message(
                    chat_id=call.from_user.id,
                    message_id=message
                )
            except ApiException:
                pass
        if self.check_user_state(call.from_user.id) == "watch_apartment":
            self.send_start_message(call.from_user.id)
        elif self.check_user_state(call.from_user.id) == "watch_apartment_cost":
            self.set_user_state(call.from_user.id, "choose_cost_range")
            text = "Выберите диапазон стоимости"
            keyboard = self.Keyboards.cost_range_keyboard("Клиент")
            self.bot.send_message(
                chat_id=call.from_user.id,
                text=text,
                reply_markup=keyboard
            )

    def send_employees_review_requests(self, call, employee_chat, review=None):
        """Виводимо заявки на огляд квартири, конкретному агенту."""
        photos = review.apartment.photo_set.all()
        if len(photos) > 10:
            photos = photos[:10]
        if photos:
            photo_path = [os.path.join(photo.apartment_photo.path) for photo in photos]
            text = "<b>Новая заявка на просмотр</b>\n\n"
            text += f"<b>{review.apartment.district}</b>\n<b>Адрес:</b> {review.apartment.address}\n<b>Количество комнат:</b> {review.apartment.room_count}\n<b>Цена:</b> {review.apartment.price_usd}$\n"
            text += f"<b>Описание:</b> {review.apartment.description}"
            if len(text) < 1024:
                ready_files = []
                first_photo = True
                for photo in photo_path:
                    if first_photo:
                        ready_files.append(types.InputMediaPhoto(open(photo, 'rb'), caption=text, parse_mode="HTML"))
                        first_photo = False
                    else:
                        ready_files.append(types.InputMediaPhoto(open(photo, 'rb')))
                self.bot.send_media_group(
                    chat_id=employee_chat,
                    media=ready_files,
                )
                text = f"<b>Телефон*:</b> {review.apartment.phone_number}\n<b>Доп.информация*:</b> {review.apartment.extra_info}\n\n\n"
                text += f"<b>Пользователь:</b> {review.customer.name}\n{review.customer.phone_number}\n"
                self.bot.send_message(
                    chat_id=employee_chat,
                    text=text,
                    parse_mode="HTML"
                )
            else:
                ready_files = []
                first_photo = True
                for photo in photo_path:
                    if first_photo:
                        ready_files.append(types.InputMediaPhoto(open(photo, 'rb'), caption=text[:1025], parse_mode="HTML"))
                        first_photo = False
                    else:
                        ready_files.append(types.InputMediaPhoto(open(photo, 'rb')))
                self.bot.send_media_group(
                    chat_id=employee_chat,
                    media=ready_files,
                )
                self.bot.send_message(
                    chat_id=employee_chat,
                    text=text[1025:]
                )
                text = f"<b>Телефон*:</b> {apartment.phone_number}\n<b>Доп.информация*:</b> {apartment.extra_info}"
                text += f"<b>Пользователь:</b> {review.customer.name}\n{review.customer.phone_number}\n"
                self.bot.send_message(
                    chat_id=employee_chat,
                    text=text,
                    parse_mode="HTML"
                )
        else:
            text = f"<b>{review.apartment.district}</b>\n<b>Адрес:</b> {review.apartment.address}\n<b>Количество комнат:</b> {review.apartment.room_count}\n<b>Цена:</b> {review.apartment.price_usd}$\n"
            text += f"<b>Описание:</b> {review.apartment.description}"
            text = f"<b>Телефон*:</b> {review.apartment.phone_number}\n<b>Доп.информация*:</b> {review.apartment.extra_info}\n\n\n"
            text += f"<b>Пользователь:</b> {review.customer.name}\n{review.customer.phone_number}\n"
            self.bot.send_message(
                chat_id=employee_chat,
                text=text,
                parse_mode="HTML"
            )

    def apartments_array_to_send(self, telegram_id, cost_range=None):
        """Надсилаємо усі фото і детальну інфо про конкретну квартиру."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        user_liked = user.interested_dist
        self.change_price_to_all_apartments(user_liked)
        if cost_range:
            if user.cost_range == "VIP":
                apartments = []
                all_apartments = Apartment.objects.filter(district=user_liked, is_active=True).order_by("-pk")
                for apartment in all_apartments:
                    if int(apartment.price_usd) > 2000:
                        apartments.append(apartment)
            else:
                user_cost_range = user.cost_range.split("-")
                cost_from = int(user_cost_range[0])
                cost_to = int(user_cost_range[-1])
                apartments = []
                all_apartments = Apartment.objects.filter(district=user_liked, is_active=True).order_by("-pk") #
                for apartment in all_apartments:
                    if int(apartment.price_usd) in range(cost_from, cost_to+1):
                        apartments.append(apartment)
        else:
            apartments = Apartment.objects.filter(district=user_liked, is_active=True).order_by("-pk") #
        if apartments:
            messages = ""
            for apartment in apartments:
                photos = apartment.photo_set.all()
                if len(photos) > 10:
                    photos = photos[:10]
                photo_path = [os.path.join(photo.apartment_photo.path) for photo in photos]
                text = f"<b>{apartment.district}.</b>\n<b>Адрес:</b> {apartment.address}. "
                text += f"<b>Количество комнат:</b> {apartment.room_count}. <b>Цена:</b> {apartment.price_usd}$"
                ready_files = []
                first_photo = True
                for photo in photo_path:
                    if first_photo:
                        ready_files.append(types.InputMediaPhoto(open(photo, 'rb')))
                        first_photo = False
                    else:
                        ready_files.append(types.InputMediaPhoto(open(photo, 'rb')))
                a = self.bot.send_media_group(
                    chat_id=telegram_id,
                    media=ready_files,
                )
                for i in a:
                    messages += f"{i.message_id}|"
                b = self.bot.send_message(
                    chat_id=telegram_id,
                    text=text,
                    reply_markup=self.Keyboards.detail_and_review_keyboard(apartment.id),
                    parse_mode="HTML"
                )
                messages += f"{b.message_id}|"
                time.sleep(0.85)
            self.set_work_field_2(telegram_id, messages)
        else:
            if cost_range:
                self.set_user_state(telegram_id, "choose_cost_range")
                self.bot.send_message(
                    chat_id=telegram_id,
                    text="Пожалуйста, выберите другой диапазон цен.",
                    reply_markup=self.Keyboards.cost_range_keyboard("Клиент")
                )
            else:
                self.set_user_state(telegram_id, "regular")
                self.bot.send_message(
                    chat_id=telegram_id,
                    text="Извените, но по заданому фильтру квартиры отсутствуют.",
                    reply_markup=self.Keyboards.menu_keyboard()
                )

    def apartments_array_to_send_detail(self, telegram_id, call):
        """Надсилаємо детальну інфо про конкретну квартиру."""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        apartment_id = call.data.split()[-1]
        user_liked = user.interested_dist
        apartment = Apartment.objects.get(id=apartment_id)
        text = f"<b>{apartment.district}.</b>\n<b>Адрес:</b> {apartment.address}. "
        text += f"<b>Количество комнат:</b> {apartment.room_count}. <b>Цена:</b> {apartment.price_usd}$. "
        text += f"<b>Описание:</b> {apartment.description}"
        self.bot.edit_message_text(
            text=text,
            chat_id=telegram_id,
            message_id=call.message.message_id,
            reply_markup=self.Keyboards.review_and_back_keyboard(apartment.id),
            parse_mode="HTML"
        )
    
    def apartments_array_to_send_back(self, telegram_id, call):
        """Забираємо детальну інфо про конкретну квартиру при НАЗАД"""
        user = Telegram_user.objects.get(telegram_id=telegram_id)
        apartment_id = call.data.split()[-1]
        user_liked = user.interested_dist
        apartment = Apartment.objects.get(id=apartment_id)
        text = f"<b>{apartment.district}. Адрес:</b> {apartment.address}. <b>Количество комнат:</b> "
        text += f"{apartment.room_count}. <b>Цена:</b> {apartment.price_usd}$"
        self.bot.edit_message_text(
            text=text,
            chat_id=telegram_id,
            message_id=call.message.message_id,
            reply_markup=self.Keyboards.detail_and_review_keyboard(apartment.id),
            parse_mode="HTML"
        )

    def get_districts_query(self):
        result = []
        districts = [
            "ЖК Французский Квартал 1-2", "ЖК Преcтиж Холл", "ЖК Централ Парк/ ЖК Ричмонд",
            "ЖК Нью-Йорк", "ЖК Альтер Эго / ЖК Кардинал", "ЖК Аристократ", "ЖК Бульвар Фонтанов",
            "ЖК Чикаго / ЖК Челси Тауэр", "ЖК Тетрис Холл", "ЖК Роял Тауэр", "ЖК Джек Хауз",
            "ЖК Новопечерские Липки", "ЖК Парк Авеню", "ЖК ПечерSky / ЖК Буссов Хилл",
            "ЖК Смарт Плаза", "ЖК Хофман Хауз", "ЖК Квартет",
        ]
        count_id = 1
        for district in districts:
            result.append(
                types.InlineQueryResultArticle(
                    id=count_id,
                    title=district,
                    input_message_content=types.InputTextMessageContent(
                        district,
                        parse_mode='HTML'
                    ),
                    description=f"Выбрать",
                    thumb_height=1
                )
            )
            count_id += 1
        return result

    class Keyboards(Enum):
        """Клавіатури користувачів."""
        def menu_keyboard():
            """Клавіатура головного меню."""
            keyboard = types.InlineKeyboardMarkup()
            button1 = types.InlineKeyboardButton(text="Меню", callback_data="menu")
            keyboard.add(button1)
            return keyboard

        def menu_open_search_keyboard():
            """Клавіатура вибору району."""
            keyboard = types.InlineKeyboardMarkup()
            button1 = types.InlineKeyboardButton(text="ЖК", switch_inline_query_current_chat="ЖК")
            button2 = types.InlineKeyboardButton(text="Печерский", callback_data="Печерский")
            button3 = types.InlineKeyboardButton(text="Шевченковский", callback_data="Шевченковский")
            button4 = types.InlineKeyboardButton(text="Голосеевский", callback_data="Голосеевский")
            button5 = types.InlineKeyboardButton(text="Левый берег", callback_data="Левый берег")
            button6 = types.InlineKeyboardButton(text="Ультрамарин", callback_data="Ультрамарин")
            keyboard.row(button1)
            keyboard.row(button2)
            keyboard.row(button3)
            keyboard.row(button4)
            keyboard.row(button5)
            keyboard.row(button6)
            return keyboard

        def cost_range_keyboard(role):
            """Клавіатура вибору діапазону вартості."""
            keyboard = types.InlineKeyboardMarkup()
            if role == "Клиент":
                button1 = types.InlineKeyboardButton(text="500-1000$", callback_data="500-1000")
                button2 = types.InlineKeyboardButton(text="1000-1500$", callback_data="1000-1500")
                button3 = types.InlineKeyboardButton(text="1500-2000$", callback_data="1500-2000")
                button4 = types.InlineKeyboardButton(text="VIP", callback_data="VIP")
                button5 = types.InlineKeyboardButton(text="Назад", callback_data="Назад")
                keyboard.row(button1, button2)
                keyboard.row(button3, button4)
                keyboard.row(button5)
            else:
                button1 = types.InlineKeyboardButton(text="500-1000$", callback_data="500-1000")
                button2 = types.InlineKeyboardButton(text="1000-1500$", callback_data="1000-1500")
                button3 = types.InlineKeyboardButton(text="1500-2000$", callback_data="1500-2000")
                button4 = types.InlineKeyboardButton(text="VIP", callback_data="VIP")
                button5 = types.InlineKeyboardButton(text="Назад", callback_data="BACK_COST")
                keyboard.row(button1, button2)
                keyboard.row(button3, button4)
                keyboard.row(button5)
            return keyboard

        def detail_and_review_keyboard(apartment_id):
            """Клавіатура - Подробнее и Записаться на осмотр."""
            keyboard = types.InlineKeyboardMarkup()
            button1 = types.InlineKeyboardButton(text="Записаться на просмотр", callback_data=f"Просмотр {apartment_id}")
            button2 = types.InlineKeyboardButton(text="Подробнее", callback_data=f"Подробнее {apartment_id}")
            button3 = types.InlineKeyboardButton(text="Назад", callback_data="Back_apart")
            keyboard.row(button1, button2) #
            keyboard.row(button3)
            return keyboard

        def review_and_back_keyboard(apartment_id):
            """Клавіатура - Назад и Записаться на осмотр."""
            keyboard = types.InlineKeyboardMarkup()
            button1 = types.InlineKeyboardButton(text="Записаться на просмотр", callback_data=f"Просмотр {apartment_id}")
            button2 = types.InlineKeyboardButton(text="Назад", callback_data=f"Назад {apartment_id}")
            keyboard.row(button1)
            keyboard.row(button2)
            return keyboard
