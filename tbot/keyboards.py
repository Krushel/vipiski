from enum import Enum
from telebot import types
from .customer_bot import *
customerBot = CustomerBot()
keyboardru = {'role_menu_tourist':'Турист',
              'role_menu_agent':'Турагент',
              'role_menu_hotel':'Отель',
              'city_find':'Поиск города',
              'find_hotel':'🔎Найти отель',
              'add_cv':'🧷Добавить визитку',
              'help':'🛎Помощь',
              'propusk':'Пропустить',
              'main':'Главное меню',
              'hotel_find':'Поиск компании',
              'details':'Детальное описание',
              'employee':'Сотрудники',
              're_registrarion':'Повторная регистрация',
              'back':'Назад',
              'share_contact':'Поделиться контактом',
              'go_on':'Продолжить',}

keyboardua =  {
   "role_menu_tourist": "Турист",
   "role_menu_agent": "Турагент",
   "role_menu_hotel": "Готель",
   "city_find": "Пошук міста",
   "find_hotel": "🔎Знайти готель",
   "add_cv": "🧷Додати візитку",
   "help": "🛎Допомога",
   "propusk": "Пропустити",
   "main": "Головне меню",
   "hotel_find": "Пошук компанії",
   "details": "Детальний опис",
   "employee": "Співробітники",
   "re_registrarion": "Повторна реєстрація",
   "back": "Назад",
   "share_contact": "Поділитись контактом",
   "go_on": "Продовжити"
 }

keyboardeng =   {
   "role_menu_tourist": "Tourist",
   "role_menu_agent": "Travel agent",
   "role_menu_hotel": "Hotel",
   "city_find": "Search for my city",
   "find_hotel": "🔎Find a hotel",
   "add_cv": "🧷Add a business card",
   "help": "🛎Help",
   "propusk": "Skip",
   "main": "Main menu",
   "hotel_find": "Search for the company",
   "details": "Full description",
   "employee": "Staff",
   "re_registrarion": "Repeated registration",
   "back": "Back",
   "share_contact": "Share my contact",
   "go_on": "Next"
 }




class KeyboardsRus(Enum):
    def start_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRus = types.InlineKeyboardButton(text="🇷🇺 Русский", callback_data="registration/RUS")
        buttonUkr = types.InlineKeyboardButton(text="🇺🇦 Украинский", callback_data="registration/UKR")
        buttonEng = types.InlineKeyboardButton(text="🇺🇸 Английский", callback_data="registration/ENG")
        keyboard.add(buttonRus, buttonUkr, buttonEng)
        return keyboard

    def dogovor_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonDogovor = types.InlineKeyboardButton(text="Я согласен со всеми условиями", callback_data="dogovor")
        keyboard.add(buttonDogovor)
        return keyboard

    def dogovor_pod_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonDogovor = types.InlineKeyboardButton(text="✅Я согласен со всеми условиями", callback_data="nichego")
        keyboard.add(buttonDogovor)
        return keyboard

    def chosen_lang_keyboard():
        """Клавиатура меню языков."""

        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonDogovor = types.KeyboardButton(text=keyboardru['go_on'])
        keyboard.add(buttonDogovor)
        return keyboard

    def role_menu_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonTourist = types.InlineKeyboardButton(text=keyboardru['role_menu_tourist'], callback_data="role/Tur")
        buttonTuragent = types.InlineKeyboardButton(text=keyboardru['role_menu_agent'], callback_data="role/Oper")
        buttonHotel = types.InlineKeyboardButton(text=keyboardru['role_menu_hotel'], callback_data="role/Hotel")
        keyboard.add(buttonTourist,buttonTuragent,buttonHotel)
        return keyboard

    def share_contact():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonContact = types.KeyboardButton(text=keyboardru['share_contact'], request_contact=True)
        keyboard.add(buttonContact)
        return keyboard

    def city_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonCity = types.InlineKeyboardButton(text=keyboardru['city_find'], switch_inline_query_current_chat='')
        keyboard.add(buttonCity)
        return keyboard

    def main_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonFindHotel = types.KeyboardButton(text=keyboardru['find_hotel'])
        buttonVisitka = types.KeyboardButton(text=keyboardru['add_cv'])
        buttonHelp = types.KeyboardButton(text=keyboardru['help'])
        keyboard.row(buttonFindHotel)
        keyboard.row(buttonVisitka)
        keyboard.row(buttonHelp)
        return keyboard

    def nazad_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonNazad = types.KeyboardButton(text=keyboardru['back'])
        keyboard.add(buttonNazad)
        return keyboard

    def propustit_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonPropusk = types.KeyboardButton(text=keyboardru['propusk'])
        buttonNazad = types.KeyboardButton(text=keyboardru['back'])
        keyboard.add(buttonPropusk,buttonNazad)
        return keyboard

    def request_o_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_oper|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard

    def request_h_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_hotel|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard


    def request_h_keyboard_cv(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_hotel_cv|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard

    def request_o_detail_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text="Принять", callback_data=f"id_prin_oper|{req.id}")
        buttonOtklon = types.InlineKeyboardButton(text="Отклонить", callback_data=f"id_o_oper|{req.id}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def request_h_detail_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text="Принять", callback_data=f"id_prin_hotel|{req.id}")
        buttonOtklon = types.InlineKeyboardButton(text="Отклонить", callback_data=f"id_o_hotel|{req.id}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def back_to_main_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonMain = types.KeyboardButton(text=keyboardru['main'])
        keyboard.add(buttonMain)
        return keyboard

    def find_hotel_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonFind = types.InlineKeyboardButton(text=keyboardru['hotel_find'], switch_inline_query_current_chat='')
        keyboard.add(buttonFind)
        return keyboard

    def hotel_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text=keyboardru['details'], callback_data=f"detail|{hotel.pk}")
        keyboard.add(buttonPrinyat)
        return keyboard

    def hotel_extended_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text=keyboardru['details'], callback_data=f"detail|{hotel.pk}")
        buttonOtklon = types.InlineKeyboardButton(text=keyboardru['employee'], callback_data=f"employ|{hotel.pk}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def hotel_extended_detail_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonOtklon = types.InlineKeyboardButton(text=keyboardru['employee'], callback_data=f"employ|{hotel.pk}")
        keyboard.add(buttonOtklon)
        return keyboard

    def employees_keyboard(employee):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        if employee.facebook:
            keyboard.add(types.InlineKeyboardButton(text="Facebook", url=employee.facebook))
        if employee.instagram:
            keyboard.add(types.InlineKeyboardButton(text="Instagram", url=employee.instagram))
        # if employee.email:
        #     keyboard.add(types.InlineKeyboardButton(text="Email", url=f'mailto:{employee.email}'))
        # if employee.viber:
        #     keyboard.add(types.InlineKeyboardButton(text="Viber", url=f'https://viber://chat?number=%2B{employee.viber}'))
        if employee.whatsapp:
            keyboard.add(types.InlineKeyboardButton(text="WhatsApp", url=f'https://wa.me/{employee.whatsapp}'))
        if employee.telegram:
            keyboard.add(types.InlineKeyboardButton(text="Telegram", url=f'https://t.me/{employee.telegram}'))
        return keyboard

    def repeat_registration_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,one_time_keyboard=True)
        buttonMain = types.KeyboardButton(text=keyboardru['re_registrarion'])
        keyboard.add(buttonMain)
        return keyboard

class KeyboardsUa(Enum):
    def start_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRus = types.InlineKeyboardButton(text="🇷🇺 Русский", callback_data="registration/RUS")
        buttonUkr = types.InlineKeyboardButton(text="🇺🇦 Украинский", callback_data="registration/UKR")
        buttonEng = types.InlineKeyboardButton(text="🇺🇸 Английский", callback_data="registration/ENG")
        keyboard.add(buttonRus, buttonUkr, buttonEng)
        return keyboard

    def dogovor_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonDogovor = types.InlineKeyboardButton(text="Я згоден зі всіма умовами", callback_data="dogovor")
        keyboard.add(buttonDogovor)
        return keyboard

    def dogovor_pod_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonDogovor = types.InlineKeyboardButton(text="✅Я згоден зі всіма умовами", callback_data="nichego")
        keyboard.add(buttonDogovor)
        return keyboard

    def chosen_lang_keyboard():
        """Клавиатура меню языков."""

        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonDogovor = types.KeyboardButton(text=keyboardua['go_on'])
        keyboard.add(buttonDogovor)
        return keyboard

    def role_menu_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonTourist = types.InlineKeyboardButton(text=keyboardua['role_menu_tourist'], callback_data="role/Tur")
        buttonTuragent = types.InlineKeyboardButton(text=keyboardua['role_menu_agent'], callback_data="role/Oper")
        buttonHotel = types.InlineKeyboardButton(text=keyboardua['role_menu_hotel'], callback_data="role/Hotel")
        keyboard.add(buttonTourist, buttonTuragent, buttonHotel)
        return keyboard

    def share_contact():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonContact = types.KeyboardButton(text=keyboardua['share_contact'], request_contact=True)
        keyboard.add(buttonContact)
        return keyboard

    def city_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonCity = types.InlineKeyboardButton(text=keyboardua['city_find'], switch_inline_query_current_chat='')
        keyboard.add(buttonCity)
        return keyboard

    def main_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonFindHotel = types.KeyboardButton(text=keyboardua['find_hotel'])
        buttonVisitka = types.KeyboardButton(text=keyboardua['add_cv'])
        buttonHelp = types.KeyboardButton(text=keyboardua['help'])
        keyboard.row(buttonFindHotel)
        keyboard.row(buttonVisitka)
        keyboard.row(buttonHelp)
        return keyboard

    def nazad_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonNazad = types.KeyboardButton(text=keyboardua['back'])
        keyboard.add(buttonNazad)
        return keyboard

    def propustit_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonPropusk = types.KeyboardButton(text=keyboardua['propusk'])
        buttonNazad = types.KeyboardButton(text=keyboardua['back'])
        keyboard.add(buttonPropusk, buttonNazad)
        return keyboard

    def request_o_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_oper|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard

    def request_h_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_hotel|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard

    def request_h_keyboard_cv(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_hotel_cv|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard

    def request_o_detail_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text="Принять", callback_data=f"id_prin_oper|{req.id}")
        buttonOtklon = types.InlineKeyboardButton(text="Отклонить", callback_data=f"id_o_oper|{req.id}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def request_h_detail_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text="Принять", callback_data=f"id_prin_hotel|{req.id}")
        buttonOtklon = types.InlineKeyboardButton(text="Отклонить", callback_data=f"id_o_hotel|{req.id}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def back_to_main_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonMain = types.KeyboardButton(text=keyboardua['main'])
        keyboard.add(buttonMain)
        return keyboard

    def find_hotel_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonFind = types.InlineKeyboardButton(text=keyboardua['hotel_find'], switch_inline_query_current_chat='')
        keyboard.add(buttonFind)
        return keyboard

    def hotel_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text=keyboardua['details'], callback_data=f"detail|{hotel.yur_name}")
        keyboard.add(buttonPrinyat)
        return keyboard

    def hotel_extended_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text=keyboardua['details'], callback_data=f"detail|{hotel.yur_name}")
        buttonOtklon = types.InlineKeyboardButton(text=keyboardua['employee'], callback_data=f"employ|{hotel.yur_name}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def hotel_extended_detail_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonOtklon = types.InlineKeyboardButton(text=keyboardua['employee'], callback_data=f"employ|{hotel.yur_name}")
        keyboard.add(buttonOtklon)
        return keyboard

    def employees_keyboard(employee):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        if employee.facebook:
            keyboard.add(types.InlineKeyboardButton(text="Facebook", url=employee.facebook))
        if employee.instagram:
            keyboard.add(types.InlineKeyboardButton(text="Instagram", url=employee.instagram))
        # if employee.email:
        #     keyboard.add(types.InlineKeyboardButton(text="Email", url=f'mailto:{employee.email}'))
        # if employee.viber:
        #     keyboard.add(types.InlineKeyboardButton(text="Viber", url=f'https://viber://chat?number=%2B{employee.viber}'))
        if employee.whatsapp:
            keyboard.add(types.InlineKeyboardButton(text="WhatsApp", url=f'https://wa.me/{employee.whatsapp}'))
        if employee.telegram:
            keyboard.add(types.InlineKeyboardButton(text="Telegram", url=f'https://t.me/{employee.telegram}'))
        return keyboard

    def repeat_registration_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        buttonMain = types.KeyboardButton(text=keyboardua['re_registrarion'])
        keyboard.add(buttonMain)
        return keyboard


class KeyboardsEng(Enum):
    def start_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRus = types.InlineKeyboardButton(text="🇷🇺 Русский", callback_data="registration/RUS")
        buttonUkr = types.InlineKeyboardButton(text="🇺🇦 Украинский", callback_data="registration/UKR")
        buttonEng = types.InlineKeyboardButton(text="🇺🇸 Английский", callback_data="registration/ENG")
        keyboard.add(buttonRus, buttonUkr, buttonEng)
        return keyboard

    def dogovor_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonDogovor = types.InlineKeyboardButton(text="I agree with all of the terms", callback_data="dogovor")
        keyboard.add(buttonDogovor)
        return keyboard

    def dogovor_pod_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonDogovor = types.InlineKeyboardButton(text="✅I agree with all of the terms", callback_data="nichego")
        keyboard.add(buttonDogovor)
        return keyboard

    def chosen_lang_keyboard():
        """Клавиатура меню языков."""

        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonDogovor = types.KeyboardButton(text=keyboardeng['go_on'])
        keyboard.add(buttonDogovor)
        return keyboard

    def role_menu_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonTourist = types.InlineKeyboardButton(text=keyboardeng['role_menu_tourist'], callback_data="role/Tur")
        buttonTuragent = types.InlineKeyboardButton(text=keyboardeng['role_menu_agent'], callback_data="role/Oper")
        buttonHotel = types.InlineKeyboardButton(text=keyboardeng['role_menu_hotel'], callback_data="role/Hotel")
        keyboard.add(buttonTourist, buttonTuragent, buttonHotel)
        return keyboard

    def share_contact():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonContact = types.KeyboardButton(text=keyboardeng['share_contact'], request_contact=True)
        keyboard.add(buttonContact)
        return keyboard

    def city_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonCity = types.InlineKeyboardButton(text=keyboardeng['city_find'], switch_inline_query_current_chat='')
        keyboard.add(buttonCity)
        return keyboard

    def main_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonFindHotel = types.KeyboardButton(text=keyboardeng['find_hotel'])
        buttonVisitka = types.KeyboardButton(text=keyboardeng['add_cv'])
        buttonHelp = types.KeyboardButton(text=keyboardeng['help'])
        keyboard.row(buttonFindHotel)
        keyboard.row(buttonVisitka)
        keyboard.row(buttonHelp)
        return keyboard

    def nazad_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonNazad = types.KeyboardButton(text=keyboardeng['back'])
        keyboard.add(buttonNazad)
        return keyboard

    def propustit_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonPropusk = types.KeyboardButton(text=keyboardeng['propusk'])
        buttonNazad = types.KeyboardButton(text=keyboardeng['back'])
        keyboard.add(buttonPropusk, buttonNazad)
        return keyboard

    def request_o_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_oper|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard

    def request_h_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_hotel|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard

    def request_h_keyboard_cv(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonRabota = types.InlineKeyboardButton(text="Взять в работу", callback_data=f"id_hotel_cv|{req.id}")
        keyboard.add(buttonRabota)
        return keyboard

    def request_o_detail_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text="Принять", callback_data=f"id_prin_oper|{req.id}")
        buttonOtklon = types.InlineKeyboardButton(text="Отклонить", callback_data=f"id_o_oper|{req.id}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def request_h_detail_keyboard(req):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text="Принять", callback_data=f"id_prin_hotel|{req.id}")
        buttonOtklon = types.InlineKeyboardButton(text="Отклонить", callback_data=f"id_o_hotel|{req.id}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def back_to_main_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        buttonMain = types.KeyboardButton(text=keyboardeng['main'])
        keyboard.add(buttonMain)
        return keyboard

    def find_hotel_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonFind = types.InlineKeyboardButton(text=keyboardeng['hotel_find'], switch_inline_query_current_chat='')
        keyboard.add(buttonFind)
        return keyboard

    def hotel_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text=keyboardeng['details'],
                                                   callback_data=f"detail|{hotel.yur_name}")
        keyboard.add(buttonPrinyat)
        return keyboard

    def hotel_extended_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonPrinyat = types.InlineKeyboardButton(text=keyboardeng['details'],
                                                   callback_data=f"detail|{hotel.yur_name}")
        buttonOtklon = types.InlineKeyboardButton(text=keyboardeng['employee'],
                                                  callback_data=f"employ|{hotel.yur_name}")
        keyboard.add(buttonPrinyat, buttonOtklon)
        return keyboard

    def hotel_extended_detail_keyboard(hotel):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        buttonOtklon = types.InlineKeyboardButton(text=keyboardeng['employee'],
                                                  callback_data=f"employ|{hotel.yur_name}")
        keyboard.add(buttonOtklon)
        return keyboard

    def employees_keyboard(employee):
        """Клавиатура меню языков."""
        keyboard = types.InlineKeyboardMarkup()
        if employee.facebook:
            keyboard.add(types.InlineKeyboardButton(text="Facebook", url=employee.facebook))
        if employee.instagram:
            keyboard.add(types.InlineKeyboardButton(text="Instagram", url=employee.instagram))
        # if employee.email:
        #     keyboard.add(types.InlineKeyboardButton(text="Email", url=f'mailto:{employee.email}'))
        # if employee.viber:
        #     keyboard.add(types.InlineKeyboardButton(text="Viber", url=f'https://viber://chat?number=%2B{employee.viber}'))
        if employee.whatsapp:
            keyboard.add(types.InlineKeyboardButton(text="WhatsApp", url=f'https://wa.me/{employee.whatsapp}'))
        if employee.telegram:
            keyboard.add(types.InlineKeyboardButton(text="Telegram", url=f'https://t.me/{employee.telegram}'))
        return keyboard

    def repeat_registration_keyboard():
        """Клавиатура меню языков."""
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        buttonMain = types.KeyboardButton(text=keyboardeng['re_registrarion'])
        keyboard.add(buttonMain)
        return keyboard





keyboardsRu = {'nazad':KeyboardsRus.nazad_keyboard(),
               'start':KeyboardsRus.start_keyboard(),
                'chosen_lang':KeyboardsRus.chosen_lang_keyboard(),
               'dogovor':KeyboardsRus.dogovor_keyboard(),
               'dogovor_pod':KeyboardsRus.dogovor_pod_keyboard(),
               'role_menu':KeyboardsRus.role_menu_keyboard(),
               'contact':KeyboardsRus.share_contact(),
               'city':KeyboardsRus.city_keyboard(),
               'fio':KeyboardsRus.nazad_keyboard(),
               'yur':KeyboardsRus.nazad_keyboard(),
               'email':KeyboardsRus.nazad_keyboard(),
               'end':KeyboardsRus.nazad_keyboard(),
               'main':KeyboardsRus.main_keyboard(),
               'contact/o':KeyboardsRus.share_contact(),
               'city/o':KeyboardsRus.city_keyboard(),
               'yur/o':KeyboardsRus.nazad_keyboard(),
               'email/o':KeyboardsRus.nazad_keyboard(),
               'name/o':KeyboardsRus.nazad_keyboard(),
               'dolzhnost/o':KeyboardsRus.nazad_keyboard(),
               'site/o':KeyboardsRus.propustit_keyboard(),
               'image/o':KeyboardsRus.nazad_keyboard(),
               'end/o':KeyboardsRus.nazad_keyboard(),
               'contact/h':KeyboardsRus.nazad_keyboard(),
               'city/h':KeyboardsRus.city_keyboard(),
               'yur/h':KeyboardsRus.nazad_keyboard(),
               'email/h':KeyboardsRus.nazad_keyboard(),
               'telegram/h':KeyboardsRus.nazad_keyboard(),
               'dolzhnost/h':KeyboardsRus.nazad_keyboard(),
               'site/h':KeyboardsRus.propustit_keyboard(),
               'image/h':KeyboardsRus.nazad_keyboard(),
               'end/h':KeyboardsRus.nazad_keyboard(),
               'to_main':KeyboardsRus.back_to_main_keyboard(),
               'find_hotel2':KeyboardsRus.find_hotel_keyboard(),
               }
keyboardsUa = {'nazad':KeyboardsUa.nazad_keyboard(),
               'start':KeyboardsUa.start_keyboard(),
                'chosen_lang':KeyboardsUa.chosen_lang_keyboard(),
               'dogovor':KeyboardsUa.dogovor_keyboard(),
               'dogovor_pod':KeyboardsUa.dogovor_pod_keyboard(),
               'role_menu':KeyboardsUa.role_menu_keyboard(),
               'contact':KeyboardsUa.share_contact(),
               'city':KeyboardsUa.city_keyboard(),
               'fio':KeyboardsUa.nazad_keyboard(),
               'yur':KeyboardsUa.nazad_keyboard(),
               'email':KeyboardsUa.nazad_keyboard(),
               'end':KeyboardsUa.nazad_keyboard(),
               'main':KeyboardsUa.main_keyboard(),
               'contact/o':KeyboardsUa.share_contact(),
               'city/o':KeyboardsUa.city_keyboard(),
               'yur/o':KeyboardsUa.nazad_keyboard(),
               'email/o':KeyboardsUa.nazad_keyboard(),
               'name/o':KeyboardsUa.nazad_keyboard(),
               'dolzhnost/o':KeyboardsUa.nazad_keyboard(),
               'site/o':KeyboardsUa.propustit_keyboard(),
               'image/o':KeyboardsUa.nazad_keyboard(),
               'end/o':KeyboardsUa.nazad_keyboard(),
               'contact/h':KeyboardsUa.nazad_keyboard(),
               'city/h':KeyboardsUa.city_keyboard(),
               'yur/h':KeyboardsUa.nazad_keyboard(),
               'email/h':KeyboardsUa.nazad_keyboard(),
               'telegram/h':KeyboardsUa.nazad_keyboard(),
               'dolzhnost/h':KeyboardsUa.nazad_keyboard(),
               'site/h':KeyboardsUa.propustit_keyboard(),
               'image/h':KeyboardsUa.nazad_keyboard(),
               'end/h':KeyboardsUa.nazad_keyboard(),
               'to_main':KeyboardsUa.back_to_main_keyboard(),
               'find_hotel2':KeyboardsUa.find_hotel_keyboard(),
               }
keyboardsEng = {'nazad':KeyboardsEng.nazad_keyboard(),
               'start':KeyboardsEng.start_keyboard(),
                'chosen_lang':KeyboardsEng.chosen_lang_keyboard(),
               'dogovor':KeyboardsEng.dogovor_keyboard(),
               'dogovor_pod':KeyboardsEng.dogovor_pod_keyboard(),
               'role_menu':KeyboardsEng.role_menu_keyboard(),
               'contact':KeyboardsEng.share_contact(),
               'city':KeyboardsEng.city_keyboard(),
               'fio':KeyboardsEng.nazad_keyboard(),
               'yur':KeyboardsEng.nazad_keyboard(),
               'email':KeyboardsEng.nazad_keyboard(),
               'end':KeyboardsEng.nazad_keyboard(),
               'main':KeyboardsEng.main_keyboard(),
               'contact/o':KeyboardsEng.share_contact(),
               'city/o':KeyboardsEng.city_keyboard(),
               'yur/o':KeyboardsEng.nazad_keyboard(),
               'email/o':KeyboardsEng.nazad_keyboard(),
               'name/o':KeyboardsEng.nazad_keyboard(),
               'dolzhnost/o':KeyboardsEng.nazad_keyboard(),
               'site/o':KeyboardsEng.propustit_keyboard(),
               'image/o':KeyboardsEng.nazad_keyboard(),
               'end/o':KeyboardsEng.nazad_keyboard(),
               'contact/h':KeyboardsEng.nazad_keyboard(),
               'city/h':KeyboardsEng.city_keyboard(),
               'yur/h':KeyboardsEng.nazad_keyboard(),
               'email/h':KeyboardsEng.nazad_keyboard(),
               'telegram/h':KeyboardsEng.nazad_keyboard(),
               'dolzhnost/h':KeyboardsEng.nazad_keyboard(),
               'site/h':KeyboardsEng.propustit_keyboard(),
               'image/h':KeyboardsEng.nazad_keyboard(),
               'end/h':KeyboardsEng.nazad_keyboard(),
               'to_main':KeyboardsEng.back_to_main_keyboard(),
               'find_hotel2':KeyboardsEng.find_hotel_keyboard(),
               }

def getKeyboard(chat_id,nz=False, req=False, hot=False, main=False, h=False):
    state = customerBot.check_user_state(telegram_id=chat_id)
    lang = customerBot.check_user_lang(telegram_id=chat_id)
    if hot == True:
        return KeyboardsRus.request_h_keyboard(req)
    elif req:
        return KeyboardsRus.request_o_keyboard(req)
    if lang == 'RUS':
        if nz:
            return keyboardsRu['nazad']
        elif main:
            return keyboardsRu['to_main']
        elif h:
            return KeyboardsRus.hotel_extended_detail_keyboard(hotel=h)
        return keyboardsRu[state]
    elif lang == 'UAH':
        if nz:
            return keyboardsUa['nazad']
        elif main:
            return keyboardsUa['to_main']
        elif h:
            return KeyboardsUa.hotel_extended_detail_keyboard(hotel=h)
        return keyboardsUa[state]
    elif lang == 'ENG':
        if nz:
            return keyboardsEng['nazad']
        elif main:
            return keyboardsEng['to_main']
        elif h:
            return KeyboardsEng.hotel_extended_detail_keyboard(hotel=h)
        return keyboardsEng[state]
