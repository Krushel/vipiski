import os, requests
from django.db import models
from django.conf import settings
import telebot
from pyngrok import ngrok

class BotConfig(models.Model):
    token = models.CharField(max_length=100, blank=True, default='')
    is_active = models.BooleanField(default=False)
    server_url = models.CharField(max_length=200, blank=True, default='')

    objects = models.Manager()

    def get_me(self):
        tbot = telebot.TeleBot(self.token)
        bot_name = tbot.get_me()
        return bot_name.username

    def set_hook(self):
        bot = telebot.TeleBot(self.token)
        webhook_url = self.server_url + '/get_hook/'


        # WEBHOOK_SSL_CERT = '/etc/letsencrypt/archive/sunshine.net.ua/csr.pem'
        # WEBHOOK_SSL_PRIV = '/etc/letsencrypt/archive/my-host.duckdns.org/privkey1.pem'
        # bot.set_webhook(webhook_url, certificate=open(WEBHOOK_SSL_CERT, 'r'))
        bot.set_webhook(webhook_url)


    def save(self, *args, **kwargs):
        self.set_hook()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.get_me()

# class BotConfig(models.Model):
#     token = models.CharField(max_length=100, blank=True, default="")
#     is_active = models.BooleanField(default=False)
#
#     def get_me(self):
#         tbot = telebot.TeleBot(self.token)
#         bot_name = tbot.get_me()
#         return bot_name.username
#
#     def set_hook(self, server_url):
#         bot = telebot.TeleBot(self.token)
#         webhook_url = server_url + '/get_hook/'
#         if settings.DEV_MODE:
#             bot.set_webhook(webhook_url)
#         else:
#             #ssl_cert = os.path.join(settings.BASE_DIR, 'nginx-selfsigned.pem')  # Путь к сертификату
#             #bot.set_webhook(webhook_url, certificate=open(ssl_cert, 'r'))
#             bot.set_webhook(webhook_url)
#
#     def set_ngrok(self):
#         server_url = ngrok.connect(port=8000).replace('http', 'https')
#         print(server_url)
#         self.set_hook(server_url)
#
#     def set_active_config(self):
#         if self.is_active:
#             other_active_configs = BotConfig.objects.filter(is_active=True)
#             for config in other_active_configs:
#                 if config.pk != self.pk:
#                     config.is_active = False
#                     config.save()
#
#     def save(self, *args, **kwargs):
#         if settings.DEV_MODE:
#             self.set_ngrok()
#         else:
#             server_url = 'https://' + os.getenv('DOMAIN')
#             self.set_hook(server_url)
#         self.set_active_config()
#         super(BotConfig, self).save(*args, **kwargs)
#
#     def __str__(self):
#         return self.get_me()
