import telebot
from enum import Enum
from . models import BotConfig
from django.db import connection

class TBot:
    HEADERS = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
    }

    def __init__(self):
        if 'tbot_botconfig' in connection.introspection.table_names() and BotConfig.objects.filter(is_active=True):
            config = BotConfig.objects.get(is_active=True)
            self.bot = telebot.TeleBot(config.token)
        else:
            self.bot = telebot.TeleBot("default_token")
    def update(self, json_data):
        return telebot.types.Update.de_json(json_data)

    class Keyboards(Enum):
        pass




    
    
        
