from .customer_bot import CustomerBot

customerBot = CustomerBot()

textRu = {'start': "Приветствую! Меня зовут бот - я помогу тебе найти контакты нужной компании.",
          'start2': 'Для начала выберите язык интерфейса:',
          'dogovor': "Перед началом, ознакомьтесь с <a href='http://www.example.com/'>договором оферты</a>, <a href='http://www.example.com/'>политикой обработки персональных данных</a> и подтвердите согласие",
          'dogovor_pod_spasibo': "Спасибо",
          'role_menu': 'Для начала предлагаю опредилиться с типом вашей занятости:',
          'contact': '<b>Регистрация. Шаг 1 из 4.</b>',
          'contact2': 'Отлично! Поделитесь пожалуйста своим контактом - этот контакт будет основным идентификатором Вашего аккаунта.',
          'info': 'Информация о вас',
          'info_contact': 'Контакт: ',
          'info_city': 'Город: ',
          'info_fio': 'ФИО: ',
          'info_email': 'Email: ',
          'info_yur': 'Юр. наименование: ',
          'info_agent_name': 'Название агентства: ',
          'info_dolzhnost': 'Должность: ',
          'info_site': 'Сайт: ',
          'info_hotel_name': 'Название отеля: ',
          'info_telegram': 'Телеграм: ',
          'city': '<b>Регистрация. Шаг 2 из 4.</b>',
          'city2': 'Выберите Ваш город из списка - нажмите "Поиск города"',
          'fio': '<b>Регистрация. Шаг 3 из 4.</b>',
          'fio2': 'Введите Ваше ФИО',
          'email': '<b>Регистрация. Шаг 4 из 4.</b>',
          'email2': 'Укажите Ваш email',
          'end': 'Регистрация успешно завершена!',
          'end2': 'Теперь Вам доступен весь функционал бота.',
          'main_hotel': '<b>🔎Найти отель</b>',
          'main_hotel2': 'Открыть список доступных компаний, ознакомиться с описанием и получить необходимые контакты',
          'main_cv': '<b>🧷Добавить визитку</b>',
          'main_cv2': 'Сфотографируйте визитку отеля, которого нет в списке - наш модератор в скором времени внесет отель в список',
          'main_help': '<b>🛎Помощь</b>',
          'main_help2': 'Часто задаваемые вопросы и связь с поддержкой',
          'contact/o': '<b>Регистрация. Шаг 1 из 8.</b>',
          'contact/o2': 'Отлично! Поделитесь пожалуйста своим контактом - этот контакт будет основным идентификатором Вашего аккаунта.',
          'city/o': '<b>Регистрация. Шаг 2 из 8.</b>',
          'city/o2': 'Выберите Ваш город из списка - нажмите "Поиск города"',
          'yur/o': '<b>Регистрация. Шаг 3 из 8.</b>',
          'yur/o2': 'Укажите Ваше юридическое наименование',
          'email/o': '<b>Регистрация. Шаг 4 из 8.</b>',
          'email/o2': 'Укажите Ваш Email',
          'name/o': '<b>Регистрация. Шаг 5 из 8.</b>',
          'name/o2': 'Укажите название Вашего агентства',
          'dolzhnost/o': '<b>Регистрация. Шаг 6 из 8.</b>',
          'dolzhnost/o2': 'Укажите Вашу должность',
          'site/o': '<b>Регистрация. Шаг 7 из 8.</b>',
          'site/o2': 'Укажите Ваш сайт',
          'image/o': '<b>Регистрация. Шаг 8 из 8.</b>',
          'image/o2': 'Отправьте фото Вашей визитки',
          'city/h': '<b>Регистрация. Шаг 1 из 8.</b>',
          'city/h2': 'Выберите город из списка в котором расположен отель - нажмите "Поиск города"',
          'yur/h': '<b>Регистрация. Шаг 2 из 8.</b>',
          'yur/h2': 'Укажите название отеля на английском',
          'site/h': '<b>Регистрация. Шаг 3 из 8.</b>',
          'site/h2': 'Укажите сайт отеля, если есть',
          'contact/h': '<b>Регистрация. Шаг 4 из 8.</b>',
          'contact/h2': 'Поделитесь номером телефона контактного лица',
          'dolzhnost/h': '<b>Регистрация. Шаг 5 из 8.</b>',
          'dolzhnost/h2': 'Укажите должность контактного лица',
          'telegram/h': '<b>Регистрация. Шаг 6 из 8.</b>',
          'telegram/h2': 'Укажите логин Telegram аккаунта для связи',
          'email/h': '<b>Регистрация. Шаг 7 из 8.</b>',
          'email/h2': 'Укажите официальный Email отеля',
          'image/h': '<b>Регистрация. Шаг 8 из 8.</b>',
          'image/h2': 'Отправьте фото Вашей визитки',
          'last': '<b>Регистрация успешно завершена!</b>',
          'last2': 'Ваши данные отправлены на модерацию - после подтверждения Вам будет открыт весь функционал бота.',
          'accepted': 'Вы успешно прошли модерацию и теперь Вам доступен весь функционал бота',
          'denied': 'Вы не прошли модерацию!',
          'denied2': 'Причина:',
          'find_hotel': 'Нажмите "Главное меню" для возврата.',
          'find_hotel2': 'Для выбора необходимой компании нажмите "Поиск компании" и начните вводить название на английском языке.',
          'add_cv': 'Для добавления отеля в список, отправьте пожалуйста фото визитки',
          'add_cv2': 'Благодарим Вас! В ближайшее время мы рассмотрим визитку отеля и добавим его в список.',
          'help': 'Если Вы обнаружили ошибки, не нашли нужный отель или у Вас есть замечания по работе бота, напишите пожалуйста сюда @denys_shch',
          'error_site': 'Вы указали некорректную ссылку на сайт.  Введите корректную ссылку.',
          'error_email': 'Вы указали некорректную почту.  Введите корректную почту.',
          'error_phone': 'Вы указали некорректный номер телефона.  Введите корректный телефон.',
          'error_hotel': 'Вы указали некорректное название.  Введите название отеля на английском языке.',
          'error_fio': 'Вы указали некорректное ФИО.  Введите ваши фамилию, имя и отчество',
          "ne_podpisan": "Сперва подтвердите Ваше согласие со всеми условиями",
          "no_hotel": "Такого отеля нет в нашей базе",
          "no_hotel_desc": "Но вы можете добавить его визитку",
          "no_hotel_answ": "Добавить визитку отеля",
          }

textUa = {
    "start": "Вітаю! Мене звати бот - я допоможу вам знайти контакти потрібної компанії",
    "start2": "Для початку виберіть мову інтерфейсу:",
    "dogovor": "Перед початком, ознайомтесь з <a href='http://www.example.com/'>договором оферти</a>, <a href='http://www.example.com/'>політикою обробки персональних данних</a> і підтвердіть згоду",
    "dogovor_pod_spasibo": "Дякую",
    "role_menu": "Для початку пропоную оприділитись з типом вашої зайнятості",
    "contact": "<b>Реєстрація. Крок 1 із 4.</b>",
    "contact2": "Супер! Поділіться, будь ласка, своїм контактом - цей контакт буде основним ідентифікатором вашого аккаунту.",
    "info": "Інформація про вас",
    "info_contact": "Контакт: ",
    "info_city": "Місто: ",
    "info_fio": "ПІБ: ",
    "info_email": "Email: ",
    "info_yur": "Юр. найменування: ",
    "info_agent_name": "Назва агенства: ",
    "info_dolzhnost": "Посада: ",
    "info_site": "Сайт: ",
    "info_hotel_name": "Назва готелю: ",
    "info_telegram": "Телеграм: ",
    "city": "<b>Реєстрація. Крок 2 з 4.</b>",
    "city2": 'Виберіть ваше місто зі списку - натисніть "Пошук міста"',
    "fio": "<b>Реєстрація. Крок 3 з 4.</b>",
    "fio2": "Введіть ваше ПІБ:",
    "email": "<b>Реєстрація. Крок 4 з 4.</b>",
    "email2": "Вкажіть ваш email",
    "end": "Реєстрація пройшла успішно!",
    "end2": "Тепер вам став доступний весь функціонал боту",
    "main_hotel": "<b>🔎Знайти готель</b>",
    "main_hotel2": "Відкрити список доступних компаній, ознайомитись з описом і отримати необхідні контакти",
    "main_cv": "<b>🧷Додати візитку</b>",
    "main_cv2": "Сфотографуйте візитку готелю, якого немає в списку - наш модератор ближнім часом внесе готель в список",
    "main_help": "<b>🛎Допомога</b>",
    "main_help2": "Часто виникаючі питання і чат з підтримкою",
    'contact/o': '<b>Реєстрація. Крок 1 из 8.</b>',
    'contact/o2': 'Супер! Поділіться, будь ласка, своїм контактом - цей контакт буде основним ідентифікатором вашого аккаунту.',
    "yur/o": "<b>Реєстрація. Крок 3 из 8.</b>",
    "yur/o2": "Вкажіть ваше юридичне найменування",
    'email/o': '<b>Реєстрація. Крок 4 из 8.</b>',
    'email/o2': 'Вкажіть ваш Email',
    "yur/h": "<b>Реєстрація. Крок 2 з 8.</b>",
    "yur/h2": "Вкажіть назву готелю англійською",
    "name/o": "<b>Реєстрація. Крок 5 з 8.</b>",
    "name/o2": "Вкажіть назву вашего агенства",
    "dolzhnost/o": "<b>Реєстрація. Крок 6 из 8.</b>",
    "dolzhnost/o2": "Вкажіть вашу посаду",
    "dolzhnost/h": "<b>Реєстрація. Крок 5 з 8.</b>",
    "dolzhnost/h2": "Вкажіть посаду контактного лиця",
    "site/o": "<b>Реєстрація. Крок 7 з 8.</b>",
    "site/o2": "Вкажіть ваш сайт",
    "site/h": "<b>Реєстрація. Крок 3 з 8.</b>",
    "site/h2": "Вкажіть сайт готелю, якщо є",
    'contact/h': '<b>Реєстрація. Крок 4 з 8.</b>',
    'contact/h2': 'Поділіться номером телефону контактного лиця',
    "image/o": "<b>Реєстрація. Крок 8 з 8.</b>",
    "image/o2": "Відправте фото вашої візитки",
    "image/h": "<b>Реєстрація. Крок 8 з 8.</b>",
    "image/h2": "Відправте фото вашої візитки",
    'city/h': '<b>Реєстрація. Крок 1 з 8.</b>',
    'city/h2': 'Виберіть ваше місто зі списку - натисніть "Пошук міста"',
    'city/o': '<b>Реєстрація. Крок 2 з 8.</b>',
    'city/o2': 'Виберіть ваше місто зі списку - натисніть "Пошук міста"',
    "telegram/h": "<b>Реєстрація. Крок 6 з 8.</b>",
    "telegram/h2": "Вкажіть логін Telegram аккаунта для зв'язку",
    'email/h': '<b>Реєстрація. Крок 7 з 8.</b>',
    'email/h2': 'Вкажіть офіційний Email готеля',
    "last": "<b>Реєстрація пройшла успішно!</b>",
    "last2": "Ваші данні відправлені на модерацію - після підтвердження вам стане відкритий весь функціонал бота",
    "accepted": "Ви успішно пройшли модерацію і тепер вам доступний весь функціонал бота",
    "denied": "Ви не пройшли модерацію!",
    "denied2": "Причина:",
    "find_hotel": 'Натисніть на "Головне меню" для повернення.',
    "find_hotel2": 'Для вибору необхідної клмпанії натисніть "Пошук компанії" і почніть вводити назву англійською мовою.',
    "add_cv": "Для додання готеля в список, відправте, будь ласка, фото візитки",
    "add_cv2": "Вдячні вам! Найближчим часом ми розглянемо візитку готелю і додамо його в список",
    "help": "Якщо ви виявили помилки, не знайшли потрібний готель чи у вас є зауваження по роботі бота, напишіть, будь ласка, сюди - @denys_shch",
    "error_site": "Ви вказали некоректне посилання на сайт.  Введіть правильне посилання.",
    "error_email": "Ви вказали некоректну пошту.  Введіть правильну пошту.",
    "error_phone": "Ви вказали некоректний номер телефону.  Введіть правильний телефон.",
    "error_hotel": "Ви вказали некоректну назву.  Введіть назву готелю англійською",
    'error_fio': 'Ви вказали некоректне ПІБ. Введіть ваші прізвище, ім`я та батькові',
    "ne_podpisan": "Спершу підтвердіть Вашу згоду з усіма умовами",
    "no_hotel": "Такого готелю немає в нашій базі",
    "no_hotel_desc": "Але ви можете додати його візитку",
    "no_hotel_answ": "Додати візитку готелю",
}

textEng = {
    "start": "Hi! My name is bot and I am here to help you to find contacts of the needed company.",
    "start2": "Firstly, choose a language of the interface, please:",
    "dogovor": "Before we start, check the <a href='http://www.example.com/'>contract offer</a>, <a href='http://www.example.com/'>personal data processing policy</a> and confirm your consent, please",
    "dogovor_pod_spasibo": "Thank you",
    "role_menu": "You have to choose a type of your future account at first:",
    "contact": "<b>Registration. Step 1 of 4.</b>",
    "contact2": "Great! Now share your contact, please - this contact will be the main identifier of your account.",
    "info": "Information about you",
    "info_contact": "Contact: ",
    "info_city": "City: ",
    "info_fio": "Full name: ",
    "info_email": "Email: ",
    "info_yur": "Legal name: ",
    "info_agent_name": "Name of agency: ",
    "info_dolzhnost": "Position: ",
    "info_site": "Site: ",
    "info_hotel_name": "Hotel name: ",
    "info_telegram": "Telegram: ",
    "city": "<b>Registration. Step 2 of 4.</b>",
    "city2": 'Choose your city from the list - click "Search city"',
    "fio": "<b>Registration. Step 3 of 4.</b>",
    "fio2": "Enter your full name:",
    "email": "<b>Registration. Step 4 of 4.</b>",
    "email2": "Send me your email",
    "end": "<b>Registration was completed successfully!</b>",
    "end2": "Now, all functions of this bot are available for you.",
    "main_hotel": "<b>🔎Find a hotel</b>",
    "main_hotel2": "Open the list of available companies, check their descriptions and get needed contacts",
    "main_cv": "<b>🧷Add business card</b>",
    "main_cv2": "Take a photo of the business card of your hotel, which is not meant in the list - our moderator will add your hotel to the list as soon as possible.",
    "main_help": "<b>🛎Help</b>",
    "main_help2": "Frequent questions and communication with support",
    'contact/o': '<b>Registration. Step 1 of 8.</b>',
    'contact/o2': 'Great! Now share your contact, please - this contact will be the main identifier of your account.',
    "yur/o": "<b>Registration. Step 3 of 8.</b>",
    "yur/o2": "State your legal name, please:",
    'email/o': '<b>Registration. Step 4 of 8.</b>',
    'email/o2': 'Tell me email, please',
    "yur/h": "<b>Registration. Step 2 of 8.</b>",
    "yur/h2": "State the name of your hotel in English",
    "name/o": "<b>Registration. Step 5 of 8.</b>",
    "name/o2": "Send me the name of your agency",
    "dolzhnost/o": "<b>Registration. Step 6 of 8.</b>",
    "dolzhnost/o2": "State your position: ",
    "dolzhnost/h": "<b>Registration. Step 5 of 8.</b>",
    "dolzhnost/h2": "State the position of the contact person",
    "site/o": "<b>Registration. Step 7 of 8.</b>",
    "site/o2": "State your site link, please",
    "site/h": "<b>Registration. Step 3 of 8.</b>",
    "site/h2": "State the hotel site, if exists",
    'contact/h': '<b>Registration. Step 4 of 8.</b>',
    'contact/h2': 'Tell me the phone number of your contact person',
    "image/o": "<b>Registration. Step 8 of 8.</b>",
    "image/o2": "Send me the photo of your business card",
    "image/h": "<b>Registration. Step 8 of 8.</b>",
    "image/h2": "Send a photo of your business card",
    'city/h': '<b>Registration. Step 2 of 8.</b>',
    'city/h2': 'Choose the city from the list, where your hotel is located in - click "Search for the city"',
    'city/o': '<b>Registration. Step 2 of 8.</b>',
    'city/o2': 'Choose your city from the list - click "Search city"',
    "telegram/h": "<b>Registration. Step 6 of 8.</b>",
    "telegram/h2": "Send me the contact Telegram login ",
    'email/h': '<b>Registration. Step 7 of 8.</b>',
    'email/h2': 'State the official Email of the hotel',
    "last": "<b>Registration was completed successfully!</b>",
    "last2": "Your request is on moderation - all functions of this bot will be available for you after applying your request",
    "accepted": "You have successfully finished the moderation process, and now all functions of the bot are available for you",
    "denied": "You have not passed the moderation!",
    "denied2": "Reason:",
    "find_hotel": 'Click "Main menu" to return.',
    "find_hotel2": 'You need to click on "Search for the company" and start typing the name in English, in order to choose needed hotel.',
    "add_cv": "Send me the business card of your hotel to add it to the list",
    "add_cv2": "Thank you! We will check your business card and add your hotel to the list as soon as possible",
    "help": "If you have found any mistakes, not found needed hotel or any questions about bot work, send a message on this account, please - @denys_shch",
    "error_site": "You have mentioned the incorrect site link. Provide us with the correct link.",
    "error_email": "You have mentioned the incorrect email. Provide us with the correct email",
    "error_phone": "You have mentioned the incorrect phone number.  Provide us with the correct phone number.",
    "error_hotel": "You have mentioned the incorrect name. Provide us with the correct hotel name in English",
    'error_fio': 'You have entered an incorrect full name. Enter your Surname and Name',
    "ne_podpisan": "First, confirm your agreement with all the conditions",
    "no_hotel": "There is no such hotel in our base",
    "no_hotel_desc": "But you can add its business card",
    "no_hotel_answ": "Add hotel`s business card",
}


def getText(chat_id, req=False, error=False, pod=False, inln=0):
    state = customerBot.check_user_state(telegram_id=chat_id)
    lang = customerBot.check_user_lang(telegram_id=chat_id)
    if req == 'request_o':
        return ['Заявка №', 'Добавлен новый', 'Требует модерации']
    if lang == 'RUS':
        if error == 'error_phone':
            return textRu['error_phone']
        elif error == 'error_site':
            return textRu['error_site']
        elif error == 'error_email':
            return textRu['error_email']
        elif error == 'error_hotel':
            return textRu['error_hotel']
        elif error == 'error_fio':
            return textRu['error_fio']
        elif req == 'hotel':
            text = []
            text.append(textRu['info_site'])
            text.append(textRu['info_city'])
            text.append(textRu['info_telegram'])
            text.append(textRu['info_contact'])
            text.append(textRu['info_dolzhnost'])
            text.append(textRu['info_email'])

            return text
        elif pod:
            text = textRu['ne_podpisan']
            return text
        elif inln == 1:
            text = textRu['no_hotel']
            return text
        elif inln == 2:
            text = textRu['no_hotel_desc']
            return text
        elif inln == 3:
            text = textRu['no_hotel_answ']
            return text
        else:
            if state == 'start':
                text = []
                text.append(textRu['start'])
                text.append(textRu['start2'])
                return text
            elif state == 'contact':
                text = ''
                text += (textRu['contact'])
                text += '\n'
                text += (textRu['contact2'])
                return text
            elif state == 'city':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append(textRu['city'] + '\n' + textRu['city2'])
                return text
            elif state == 'fio':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append(textRu['fio'] + '\n' + textRu['fio2'])
                return text
            elif state == 'email':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append('\n' + textRu['info_fio'])
                text.append(textRu['email'] + '\n' + textRu['email2'])
                return text
            elif state == 'end':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append('\n' + textRu['info_fio'])
                text.append('\n' + textRu['info_email'])
                text.append(textRu['end'] + '\n' + textRu['end2'])
                return text
            elif state == 'contact/o':
                text = ''
                text += (textRu['contact/o'])
                text += '\n'
                text += (textRu['contact/o2'])
                return text
            elif state == 'city/o':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append(textRu['city'] + '\n' + textRu['city2'])
                return text
            elif state == 'yur/o':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append(textRu['yur/o'] + '\n' + textRu['yur/o2'])
                return text
            elif state == 'email/o':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append('\n' + textRu['info_yur'])
                text.append(textRu['email/o'] + '\n' + textRu['email/o2'])
                return text
            elif state == 'name/o':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append('\n' + textRu['info_yur'])
                text.append('\n' + textRu['info_email'])
                text.append(textRu['name/o'] + '\n' + textRu['name/o2'])
                return text
            elif state == 'dolzhnost/o':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append('\n' + textRu['info_yur'])
                text.append('\n' + textRu['info_email'])
                text.append('\n' + textRu['info_agent_name'])
                text.append(textRu['dolzhnost/o'] + '\n' + textRu['dolzhnost/o2'])
                return text
            elif state == 'site/o':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append('\n' + textRu['info_yur'])
                text.append('\n' + textRu['info_email'])
                text.append('\n' + textRu['info_agent_name'])
                text.append('\n' + textRu['info_dolzhnost'])
                text.append(textRu['site/o'] + '\n' + textRu['site/o2'])
                return text
            elif state == 'image/o':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_city'])
                text.append('\n' + textRu['info_yur'])
                text.append('\n' + textRu['info_email'])
                text.append('\n' + textRu['info_agent_name'])
                text.append('\n' + textRu['info_dolzhnost'])
                text.append('\n' + textRu['info_site'])
                text.append(textRu['image/o'] + '\n' + textRu['image/o2'])
                return text
            elif state == 'city/h':
                text = []
                text.append(textRu['city/h'] + '\n' + textRu['city/h2'])
                return text
            elif state == 'yur/h':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_city'])
                text.append(textRu['yur/h'] + '\n' + textRu['yur/h2'])
                return text
            elif state == 'site/h':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_city'])
                text.append('\n' + textRu['info_hotel_name'])
                text.append(textRu['site/h'] + '\n' + textRu['site/h2'])
                return text
            elif state == 'contact/h':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_city'])
                text.append('\n' + textRu['info_hotel_name'])
                text.append('\n' + textRu['info_site'])
                text.append(textRu['contact/h'] + '\n' + textRu['contact/h2'])
                return text
            elif state == 'dolzhnost/h':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_city'])
                text.append('\n' + textRu['info_hotel_name'])
                text.append('\n' + textRu['info_site'])
                text.append('\n' + textRu['info_contact'])
                text.append(textRu['dolzhnost/h'] + '\n' + textRu['dolzhnost/h2'])
                return text
            elif state == 'telegram/h':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_city'])
                text.append('\n' + textRu['info_hotel_name'])
                text.append('\n' + textRu['info_site'])
                text.append('\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_dolzhnost'])
                text.append(textRu['telegram/h'] + '\n' + textRu['telegram/h2'])
                return text
            elif state == 'email/h':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_city'])
                text.append('\n' + textRu['info_hotel_name'])
                text.append('\n' + textRu['info_site'])
                text.append('\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_dolzhnost'])
                text.append('\n' + textRu['info_telegram'])
                text.append(textRu['email/h'] + '\n' + textRu['email/h2'])
                return text
            elif state == 'image/h':
                text = []
                text.append(textRu['info'] + '\n' + textRu['info_city'])
                text.append('\n' + textRu['info_hotel_name'])
                text.append('\n' + textRu['info_site'])
                text.append('\n' + textRu['info_contact'])
                text.append('\n' + textRu['info_dolzhnost'])
                text.append('\n' + textRu['info_telegram'])
                text.append('\n' + textRu['info_email'])
                text.append(textRu['image/h'] + '\n' + textRu['image/h2'])
                return text
            elif state == 'main':
                text = textRu['main_hotel'] + '\n' + textRu['main_hotel2'] + '\n\n' + textRu['main_cv'] + '\n' + textRu[
                    'main_cv2'] + '\n\n' + textRu['main_help'] + '\n' + textRu['main_help2']
                return text
            elif state == 'last':
                text = textRu['last'] + '\n' + textRu['last2']
                return text
            elif state == 'denied':
                text = textRu['denied'] + '\n' + textRu['denied2']
                return text
            else:
                return textRu[state]
    elif lang == 'UAH':
        if error == 'error_phone':
            return textUa['error_phone']
        elif error == 'error_site':
            return textUa['error_site']
        elif error == 'error_email':
            return textUa['error_email']
        elif error == 'error_hotel':
            return textUa['error_hotel']
        elif error == 'error_fio':
            return textUa['error_fio']
        elif req == 'hotel':
            text = []
            text.append(textRu['info_site'])
            text.append(textRu['info_city'])
            text.append(textRu['info_telegram'])
            text.append(textRu['info_contact'])
            text.append(textRu['info_dolzhnost'])
            text.append(textRu['info_email'])
            return text
        elif pod:
            text = textUa['ne_podpisan']
            return text
        elif inln == 1:
            text = textUa['no_hotel']
            return text
        elif inln == 2:
            text = textUa['no_hotel_desc']
            return text
        elif inln == 3:
            text = textUa['no_hotel_answ']
            return text
        else:
            if state == 'start':
                text = []
                text.append(textUa['start'])
                text.append(textUa['start2'])
                return text
            elif state == 'contact':
                text = ''
                text += (textUa['contact'])
                text += '\n'
                text += (textUa['contact2'])
                return text
            elif state == 'city':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append(textUa['city'] + '\n' + textUa['city2'])
                return text
            elif state == 'fio':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append(textUa['fio'] + '\n' + textUa['fio2'])
                return text
            elif state == 'email':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append('\n' + textUa['info_fio'])
                text.append(textUa['email'] + '\n' + textUa['email2'])
                return text
            elif state == 'end':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append('\n' + textUa['info_fio'])
                text.append('\n' + textUa['info_email'])
                text.append(textUa['end'] + '\n' + textUa['end2'])
                return text
            elif state == 'contact/o':
                text = ''
                text += (textUa['contact/o'])
                text += '\n'
                text += (textUa['contact/o2'])
                return text
            elif state == 'city/o':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append(textUa['city'] + '\n' + textUa['city2'])
                return text
            elif state == 'yur/o':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append(textUa['yur/o'] + '\n' + textUa['yur/o2'])
                return text
            elif state == 'email/o':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append('\n' + textUa['info_yur'])
                text.append(textUa['email/o'] + '\n' + textUa['email/o2'])
                return text
            elif state == 'name/o':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append('\n' + textUa['info_yur'])
                text.append('\n' + textUa['info_email'])
                text.append(textUa['name/o'] + '\n' + textUa['name/o2'])
                return text
            elif state == 'dolzhnost/o':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append('\n' + textUa['info_yur'])
                text.append('\n' + textUa['info_email'])
                text.append('\n' + textUa['info_agent_name'])
                text.append(textUa['dolzhnost/o'] + '\n' + textUa['dolzhnost/o2'])
                return text
            elif state == 'site/o':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append('\n' + textUa['info_yur'])
                text.append('\n' + textUa['info_email'])
                text.append('\n' + textUa['info_agent_name'])
                text.append('\n' + textUa['info_dolzhnost'])
                text.append(textUa['site/o'] + '\n' + textUa['site/o2'])
                return text
            elif state == 'image/o':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_city'])
                text.append('\n' + textUa['info_yur'])
                text.append('\n' + textUa['info_email'])
                text.append('\n' + textUa['info_agent_name'])
                text.append('\n' + textUa['info_dolzhnost'])
                text.append('\n' + textUa['info_site'])
                text.append(textUa['image/o'] + '\n' + textUa['image/o2'])
                return text
            elif state == 'city/h':
                text = []
                text.append(textUa['city/h'] + '\n' + textUa['city/h2'])
                return text
            elif state == 'yur/h':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_city'])
                text.append(textUa['yur/h'] + '\n' + textUa['yur/h2'])
                return text
            elif state == 'site/h':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_city'])
                text.append('\n' + textUa['info_hotel_name'])
                text.append(textUa['site/h'] + '\n' + textUa['site/h2'])
                return text
            elif state == 'contact/h':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_city'])
                text.append('\n' + textUa['info_hotel_name'])
                text.append('\n' + textUa['info_site'])
                text.append(textUa['contact/h'] + '\n' + textUa['contact/h2'])
                return text
            elif state == 'dolzhnost/h':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_city'])
                text.append('\n' + textUa['info_hotel_name'])
                text.append('\n' + textUa['info_site'])
                text.append('\n' + textUa['info_contact'])
                text.append(textUa['dolzhnost/h'] + '\n' + textUa['dolzhnost/h2'])
                return text
            elif state == 'telegram/h':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_city'])
                text.append('\n' + textUa['info_hotel_name'])
                text.append('\n' + textUa['info_site'])
                text.append('\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_dolzhnost'])
                text.append(textUa['telegram/h'] + '\n' + textUa['telegram/h2'])
                return text
            elif state == 'email/h':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_city'])
                text.append('\n' + textUa['info_hotel_name'])
                text.append('\n' + textUa['info_site'])
                text.append('\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_dolzhnost'])
                text.append('\n' + textUa['info_telegram'])
                text.append(textUa['email/h'] + '\n' + textUa['email/h2'])
                return text
            elif state == 'image/h':
                text = []
                text.append(textUa['info'] + '\n' + textUa['info_city'])
                text.append('\n' + textUa['info_hotel_name'])
                text.append('\n' + textUa['info_site'])
                text.append('\n' + textUa['info_contact'])
                text.append('\n' + textUa['info_dolzhnost'])
                text.append('\n' + textUa['info_telegram'])
                text.append('\n' + textUa['info_email'])
                text.append(textUa['image/h'] + '\n' + textUa['image/h2'])
                return text
            elif state == 'main':
                text = textUa['main_hotel'] + '\n' + textUa['main_hotel2'] + '\n\n' + textUa['main_cv'] + '\n' + textUa[
                    'main_cv2'] + '\n\n' + textUa['main_help'] + '\n' + textUa['main_help2']
                return text
            elif state == 'last':
                text = textUa['last'] + '\n' + textUa['last2']
                return text
            elif state == 'denied':
                text = textUa['denied'] + '\n' + textUa['denied2']
                return text
            else:
                return textUa[state]
    elif lang == 'ENG':
        if error == 'error_phone':
            return textEng['error_phone']
        elif error == 'error_site':
            return textEng['error_site']
        elif error == 'error_email':
            return textEng['error_email']
        elif error == 'error_hotel':
            return textEng['error_hotel']
        elif error == 'error_fio':
            return textEng['error_fio']
        elif req == 'hotel':
            text = []
            text.append(textRu['info_site'])
            text.append(textRu['info_city'])
            text.append(textRu['info_telegram'])
            text.append(textRu['info_contact'])
            text.append(textRu['info_dolzhnost'])
            text.append(textRu['info_email'])
            return text
        elif pod:
            text = textEng['ne_podpisan']
            return text
        elif inln == 1:
            text = textEng['no_hotel']
            return text
        elif inln == 2:
            text = textEng['no_hotel_desc']
            return text
        elif inln == 3:
            text = textEng['no_hotel_answ']
            return text
        else:
            if state == 'start':
                text = []
                text.append(textEng['start'])
                text.append(textEng['start2'])
                return text
            elif state == 'contact':
                text = ''
                text += (textEng['contact'])
                text += '\n'
                text += (textEng['contact2'])
                return text
            elif state == 'city':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append(textEng['city'] + '\n' + textEng['city2'])
                return text
            elif state == 'fio':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append(textEng['fio'] + '\n' + textEng['fio2'])
                return text
            elif state == 'email':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append('\n' + textEng['info_fio'])
                text.append(textEng['email'] + '\n' + textEng['email2'])
                return text
            elif state == 'end':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append('\n' + textEng['info_fio'])
                text.append('\n' + textEng['info_email'])
                text.append(textEng['end'] + '\n' + textEng['end2'])
                return text
            elif state == 'contact/o':
                text = ''
                text += (textEng['contact/o'])
                text += '\n'
                text += (textEng['contact/o2'])
                return text
            elif state == 'city/o':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append(textEng['city'] + '\n' + textEng['city2'])
                return text
            elif state == 'yur/o':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append(textEng['yur/o'] + '\n' + textEng['yur/o2'])
                return text
            elif state == 'email/o':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append('\n' + textEng['info_yur'])
                text.append(textEng['email/o'] + '\n' + textEng['email/o2'])
                return text
            elif state == 'name/o':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append('\n' + textEng['info_yur'])
                text.append('\n' + textEng['info_email'])
                text.append(textEng['name/o'] + '\n' + textEng['name/o2'])
                return text
            elif state == 'dolzhnost/o':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append('\n' + textEng['info_yur'])
                text.append('\n' + textEng['info_email'])
                text.append('\n' + textEng['info_agent_name'])
                text.append(textEng['dolzhnost/o'] + '\n' + textEng['dolzhnost/o2'])
                return text
            elif state == 'site/o':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append('\n' + textEng['info_yur'])
                text.append('\n' + textEng['info_email'])
                text.append('\n' + textEng['info_agent_name'])
                text.append('\n' + textEng['info_dolzhnost'])
                text.append(textEng['site/o'] + '\n' + textEng['site/o2'])
                return text
            elif state == 'image/o':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_city'])
                text.append('\n' + textEng['info_yur'])
                text.append('\n' + textEng['info_email'])
                text.append('\n' + textEng['info_agent_name'])
                text.append('\n' + textEng['info_dolzhnost'])
                text.append('\n' + textEng['info_site'])
                text.append(textEng['image/o'] + '\n' + textEng['image/o2'])
                return text
            elif state == 'city/h':
                text = []
                text.append(textEng['city/h'] + '\n' + textEng['city/h2'])
                return text
            elif state == 'yur/h':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_city'])
                text.append(textEng['yur/h'] + '\n' + textEng['yur/h2'])
                return text
            elif state == 'site/h':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_city'])
                text.append('\n' + textEng['info_hotel_name'])
                text.append(textEng['site/h'] + '\n' + textEng['site/h2'])
                return text
            elif state == 'contact/h':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_city'])
                text.append('\n' + textEng['info_hotel_name'])
                text.append('\n' + textEng['info_site'])
                text.append(textEng['contact/h'] + '\n' + textEng['contact/h2'])
                return text
            elif state == 'dolzhnost/h':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_city'])
                text.append('\n' + textEng['info_hotel_name'])
                text.append('\n' + textEng['info_site'])
                text.append('\n' + textEng['info_contact'])
                text.append(textEng['dolzhnost/h'] + '\n' + textEng['dolzhnost/h2'])
                return text
            elif state == 'telegram/h':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_city'])
                text.append('\n' + textEng['info_hotel_name'])
                text.append('\n' + textEng['info_site'])
                text.append('\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_dolzhnost'])
                text.append(textEng['telegram/h'] + '\n' + textEng['telegram/h2'])
                return text
            elif state == 'email/h':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_city'])
                text.append('\n' + textEng['info_hotel_name'])
                text.append('\n' + textEng['info_site'])
                text.append('\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_dolzhnost'])
                text.append('\n' + textEng['info_telegram'])
                text.append(textEng['email/h'] + '\n' + textEng['email/h2'])
                return text
            elif state == 'image/h':
                text = []
                text.append(textEng['info'] + '\n' + textEng['info_city'])
                text.append('\n' + textEng['info_hotel_name'])
                text.append('\n' + textEng['info_site'])
                text.append('\n' + textEng['info_contact'])
                text.append('\n' + textEng['info_dolzhnost'])
                text.append('\n' + textEng['info_telegram'])
                text.append('\n' + textEng['info_email'])
                text.append(textEng['image/h'] + '\n' + textEng['image/h2'])
                return text
            elif state == 'main':
                text = textEng['main_hotel'] + '\n' + textEng['main_hotel2'] + '\n\n' + textEng['main_cv'] + '\n' + \
                       textEng[
                           'main_cv2'] + '\n\n' + textEng['main_help'] + '\n' + textEng['main_help2']
                return text
            elif state == 'last':
                text = textEng['last'] + '\n' + textEng['last2']
                return text
            elif state == 'denied':
                text = textEng['denied'] + '\n' + textEng['denied2']
                return text
            else:
                return textEng[state]
