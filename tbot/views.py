import os
from datetime import datetime
from time import sleep
from django.http import HttpResponse
from django.core.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt
from django.core.files.base import File
from django.conf import settings
from telebot import types
# from telegram_bot_calendar import LSTEP, WMonthTelegramCalendar
from telebot.apihelper import ApiException
from customer.models import *

from .tbot import TBot
from .customer_bot import CustomerBot
from .admin_bot import AdminBot
from .keyboards import *
from .texts import getText
from payment.payment import PaymentBot  # Импортируем класс, отвечающий за платежи
from telegram_bot_calendar import LSTEP, WMonthTelegramCalendar


pBot = PaymentBot()  # Создаём экземпляр этого класса



tBot = TBot()
customerBot = CustomerBot()
adminBot = AdminBot()




@csrf_exempt
def get_hook(request):
    print(request)
    if request.META['CONTENT_TYPE'] == 'application/json':
        json_data = request.body.decode('utf-8')
        print(json_data)
        update = tBot.update(json_data)
        tBot.bot.process_new_updates([update])
        return HttpResponse("")
    else:
        raise PermissionDenied


@tBot.bot.message_handler(content_types=['contact'])
def contact(message: types.Message):
    if customerBot.check_user_role(message.from_user.id) == 'Клиент':
        customerBot.set_user_phone(message.from_user.id, str(message.contact.phone_number))
        customerBot.set_user_state(message.from_user.id, 'dogovor')
        adminBot.send_text(message.from_user.id, 'Для продолжения примите условия пользовательского соглашения.',
                              btn='Я принимаю условия пользовательского соглашения', call='dogovor')

@tBot.bot.message_handler(content_types=['photo'])
def photo(message: types.Message):
    if customerBot.check_user_state(message.from_user.id).startswith('photo'):
        img = adminBot.get_photo(message)
        id = customerBot.check_user_state(message.from_user.id).split('|')[1]
        adminBot.send_image(id, text='Ваш сертификат готов!', photo=img, btn='Главное меню', call='back_to_menu')
        adminBot.send_text(message.from_user.id, 'Сертификат отправлен клиенту!  🙂',
                           btn='Главное меню', call='back_to_menu')
#     elif customerBot.check_user_state(message.from_user.id) == "image/h":
#         img = adminBot.get_photo(message)
#         user = customerBot.get_user(message.from_user.id)
#         customerBot.add_new_hotel_req(user=user,img=img)
#         customerBot.set_user_state(message.from_user.id, 'last')
#         adminBot.send_last_message(message.from_user.id)
#     elif customerBot.check_user_state(message.from_user.id) == "add_cv":
#         img = adminBot.get_photo(message)
#         customerBot.add_new_hotel_cv(img=img)
#         adminBot.send_hotel_request_cv(image=img)
#         customerBot.set_user_state(message.from_user.id, 'add_cv2')
#         adminBot.send_to_main_message(message.from_user.id)

@tBot.bot.message_handler(content_types=['document'])
def doc(message: types.Message):
    if customerBot.check_user_state(message.from_user.id).startswith('file'):
        chat_id = message.from_user.id
        user = customerBot.get_user(chat_id)
        img = adminBot.get_file(message)
        id = customerBot.check_user_state(message.from_user.id).split('|')[1]
        req = Req.objects.get(pk=id)
        req.file = img
        req.save()
        user.state = 'menu'
        user.save()



@tBot.bot.inline_handler(func=lambda query: query.from_user.id > 0)
def query_text(query: types.InlineQuery):
    chat_id = query.from_user.id
    user = customerBot.get_user(chat_id)
    if 'city' in customerBot.check_user_state(chat_id):
        adminBot.send_city_inline_message(query)
    # if 'street' in customerBot.check_user_state(chat_id):
    #     adminBot.send_street_inline_message(query, user)
    if 'post' in customerBot.check_user_state(chat_id):
        adminBot.send_post_inline_message(query, user)


@tBot.bot.message_handler(commands=["start"])
def start(message: types.Message):
    if customerBot.check_user_dont_exists(message):
        customerBot.add_new_user(message)
    user = customerBot.get_user(message.from_user.id)
    if user.active:
        greet = Greetings.objects.last()
        text = greet.text
        adminBot.send_text(message.chat.id, text)
        adminBot.send_text(message.chat.id, 'Для продовження використання чат-бота необхідно ознайомитися з договором '
                                            'оферти і підтвердити згоду з усіма умовами, '
                                            'натиснувши на кнопку "Згоден (-а)".', 'Согласен (-а)', 'accept')


@tBot.bot.callback_query_handler(func=lambda call: True and customerBot.check_user_role(call.from_user.id) in ('Пользователь', 'Менеджер'))
def client_callback(call: types.CallbackQuery):
    chat_id = call.from_user.id
    user = customerBot.get_user(chat_id)
    if user.active:
        if call.data.startswith('back_to_menu'):
            adminBot.send_menu(chat_id)
        elif call.data.startswith('accept'):
            adminBot.send_menu(chat_id)
        elif call.data.startswith('address'):
            user.state = 'text'
            user.type = 'Адрес'
            user.save()
            adminBot.send_text(chat_id,
                               'Обраний тип пошуку: <b>За адресою</b>\n\nВведіть адресу об`єкта, що цікавить (параметри):')
        elif call.data.startswith('r_number'):
            user.state = 'text'
            user.type = 'р номер'
            user.save()
            adminBot.send_text(chat_id, 'Обраний тип пошуку: <b>пошук за реєстраційним номером об`єкта нерухомості</b>\n\n'
                                        'Введіть реєстраційним номером об`єкта нерухомості в форматi (ХХХХХХХХ):')
        elif call.data.startswith('k_number'):
            user.state = 'text'
            user.type = 'к номер'
            user.save()
            adminBot.send_text(chat_id, 'Обраний тип пошуку: <b>пошук за кадастровим номером земельної дiлянки</b>\n'
                                        'Введіть кадастровий номер земельної дiлянки в форматi (ХХХХХХХХХХ:ХХ:ХХХ:ХХХХ):')
        elif call.data.startswith('osoba'):
            user.state = 'text'
            user.type = 'Физ'
            user.save()
            adminBot.send_text(chat_id, 'Обраний тип пошуку: <b>пошук за даними фіз. особи або юр. особи.</b>\n'
                                        'Введіть найменування юридичної особи (або ПІБ фізичної особи) та ІПН:')
        elif call.data.startswith('file'):
            print(111)
            id = call.data.split('|')[1]
            user.state = f'file|{id}'
            user.save()
            adminBot.send_text(chat_id, 'Отправьте файл')



@tBot.bot.pre_checkout_query_handler(func=lambda pre_checkout_query: True)
def pre_checkout(pre_checkout_query):
    """Responds to a request for pre-inspection"""
    chat_id = pre_checkout_query.from_user.id
    tBot.bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True)
    user = customerBot.get_user(chat_id)
    req = Req.objects.create(user=user, text=user.text, type=user.type)
    adminBot.send_req(req.id)
    if int(datetime.now().strftime('%H')) > 22 or int(datetime.now().strftime('%H')) < 8:
        adminBot.send_text(chat_id,
                           f'ID Вашої заявки: {req.id}\nДані були передані менеджеру, якщо оплату оформлено в період 22.00 - 8.00 год. - чекайте відповідь з 9.00 ранку',
                           'Главное меню', 'back_to_menu')
    else:
        adminBot.send_text(chat_id, f'ID Вашої заявки: {req.id}\nДані були передані менеджеру і протягом години Вам '
                                    f'будет відправлений .pdf файл з випискою.', 'Головне меню', 'back_to_menu')

@tBot.bot.message_handler(func=lambda message: True and customerBot.check_user_role(message.from_user.id) == 'Пользователь')
def client_messages(message: types.Message):
    chat_id = message.from_user.id
    user = customerBot.get_user(chat_id)
    if user.active:
        if message.text == '⏪':
            adminBot.send_menu(chat_id)
        elif customerBot.check_user_state(chat_id) == 'text':
            product = Product_cart.objects.last()
            user.text = message.text
            user.save()
            pBot.send_product_invoice(chat_id=chat_id, message_id=message.message_id, product=product, user_lang="ru")





